<?php
/**
 * index-json.php
 *
 * Main file to execute backgroun json requests
 *
 * @version    1.0 2014-04-18
 * @package    DRDB
 * @copyright  Copyright (c) 2014 Martin Sauter
 * @license    GNU General Public License
 * @since      Since Release 1.0
 */

// Before going any further check if the web server has authenticated a user.
if (!isset($_SERVER['PHP_AUTH_USER'])) {
	echo '<p>ERROR: No user authentication configured e.g. in apache2.conf ' .
			'or .htaccess. Aborting...</p>';
	exit;
}

require_once 'includes/init.php';

// Get the content from the url and sanitize it
$content = filter_input(INPUT_GET, 'content', FILTER_SANITIZE_STRING);

// If there's a valid filename, load it
if ($content) {
	// sanitize the data to prevent hacking.
	$html = include 'json/'.$content.'.php';
}

?>
