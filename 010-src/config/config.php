<?php 

/**
 *
 * General configuration parameters
 * 
 * @version    1.1 2021-02-20
 * @package    DRDB
 * @copyright  Copyright (c) 2017 Martin Sauter
 * @license    GNU General Public License
 * @since      Since Release 1.0
 */


//
// IMPORTANT: This file is included in init.php so the variables can be used
// everywhere! Use the following class and method to access the contents of the
// array:
//
// $result = DatabaseConfigStorage::getCfgParam('CONFIG_VARIABLE_NAME');
//
// To make the content editable to the user via the web GUI, some of these
// variables are only here for an initial configuration when the project
// is created. Once changed, they are stored in the drdb_config table!
//

$CONFIG = array (
    'version' => '1.1',    
    
    // In a Docker environment, the trusted_domains parameter
    // is taken from environment variables. Thus, this parameter
    // only applies to manual installations.
    'trusted_domains' =>
    array (
    	'www.my-domain-name.com',
        'localhost',
    ),
    
    'salt'               => 'pwsr36dVrwJx3noW',
    
    // The following configuration parameters can also be overridden by
    // environment variables in Docker environments. This is necessary
    // as the MySQL container has to crate the initial database when
    // it is first started. The user should not do this manually.
    // See Docker part of the documentation for details.
    'mysqlUser'          => 'TAKEN_FROM_DOCKER_ENV_VAR',
    'mysqlPass'          => 'TAKEN_FROM_DOCKER_ENV_VAR',
    'mysqlDb'            => 'TAKEN_FROM_DOCKER_ENV_VAR',
    'mysqlHostName'      => 'TAKEN_FROM_DOCKER_ENV_VAR',
    // End of vars with environment variable override possiblility
    
    'mysqlMainTable'     => 'drdb_main_table',
    
    'doc-name-ui'        => 'document',
    'doc-name-ui-plural' => 'documents',
    'title'              => "DRDB Demo Database",
    'footer'             => '(c) 2014-2022 Martin Sauter and Gerrit Berenike Heiter',
    
    'fix_french_punctuation'       => false,
    'protect_against_xss'          => true,
    'default_increment_new_record' => 100,
);


?>
