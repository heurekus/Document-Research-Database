<?php
/**
 * record_list_view_print.php
 *
 * Print version of a record list.
 * 
 *  * Takes URL parameters to decide what kind of document list to generate
 *  * Calls the appropriate method in the database class (which contains 
 *    the SQL statement for the query) 
 *  * Outputs the resulting record list.
 *  * Only fields that have been marked while formulating the search request
 *    are printed out. Note: This is different from the visible field list
 *    that is used when showing an individual record
 * 
 *
 * @version    1.1 2019-01-28
 * @package    DRDB
 * @copyright  Copyright (c) 2017-2019 Martin Sauter
 * @license    GNU General Public License
 * @since      Since Release 1.0
 */

require_once 'includes/field-empty-and-trim-functions.php';

// Exporting large databases to LibreOffice can require a significant
// amount of memory. The default php memory limit of 128 MB is too small.
// Let's be a bit more generous.
ini_set('memory_limit', '1024M');

$log = new Logging();
$log->lwrite('Document List View - Print page accessed');

if (!UserPermissions::hasAccess('export')) {
    $log->lwrite('Export aborted, user does not have export rights');
    return; 
}

$local_doc_db_description = DatabaseConfigStorage::getDbFieldConfig();

// Create a writer export object to generate XML formatted text from
// a database record and to hold style information.
$lo_export = new LO_Writer_Export();


// Check if the user wants output without fancy formatting
if (isset($_GET['quick_print'])) {
	$quick_print = true;
}
else {
	$quick_print = false;
}

// Select documents to export
$items = SelectDocuments();

// Write general information to the top of the document such as number
// of documents, etc.
OutputGeneralInformation($lo_export, $log, $items, $quick_print);

// Now export the records
ExportListOfDocuments($log,
		              $items, 
		              $local_doc_db_description,
		              $quick_print, 
		              $lo_export);

// Move this into a generic file transfer funnction for exported
// Libreoffice content
SendLibreofficeWriterDocOverHttp ($log, $lo_export);

// Instead of returning at this point, we exit so no additional
// content is sent.
exit();


/**
 * SelectDocuments()
 *
 * NOTE: COULD THIS BE A FUNCTION TO BE SHARED WITH THE WEB LIST VIEW???
 * 
 */

function SelectDocuments() {

	$doc_db_description = DatabaseConfigStorage::getDbFieldConfig();
		
	$banner_text = '';
	$doc_list_type = 'empty';

	// Find out what kind of list the calling page desires and act accordingly
	if (isset($_GET['doc_list_type'])) {
		$doc_list_type = $_GET['doc_list_type'];
	}

	// Find out if a case sensitive search is requested
	if (isset($_POST['case_sensitive'])) {
		$case_sensitive_search = true;
	} else {
		$case_sensitive_search = false;
	}

	// Use the getListOfRecords() to get a list of documents. All other
	// variables returned are not used.
	list ($items,
		  $banner_text,
		  $footer_text,
		  $replacement_term,
		  $fields_and_search_terms) = getListOfRecords($doc_list_type,
					                                   $doc_db_description,
					                                   $case_sensitive_search);
	return $items;
}

/**
 * CheckAndOutputSingleYearLimit()
 *
 * Checks HTTP POST variable if the output is to be limited to a single 
 * year. Return the year or 0 if no limit is to be imposed.
 * 
 */

function CheckAndOutputSingleYearLimit($log, $quick_print, &$lo_export) {
	
	$limit_to_year = 0;
	
	// Check if the user wants to limit the display to a certain year
	// (document search by decade/category)
	if (isset($_POST['limit_to_year'])) {
		$limit_to_year = (int) $_POST['limit_to_year'];
	
		if ($limit_to_year != 0) {
			$log->lwrite('List limited to ' . $limit_to_year);
	
			$str = '<p><strong>Limiting </strong> list to ' .
				   DatabaseConfigStorage::getCfgParam('doc-name-ui-plural'). ' of ' . $limit_to_year .
				   ' (and ' . DatabaseConfigStorage::getCfgParam('doc-name-ui-plural') .
				   ' without a date immediately after a ' .
				   DatabaseConfigStorage::getCfgParam('doc-name-ui') .' that matches the year)</p><p></p>';
			
			$para_format_name = "aa-1";
			if ($quick_print) $para_format_name = "Standard";
			
			$result = $lo_export->Convert_Html_To_LO_XML($str, $para_format_name);
			
			$log->lwrite($result);
		}
	}
	else {
		$limit_to_year = 0;
	}
	
	return $limit_to_year;
}

/**
 * ExportListOfDocuments()
 *
 *
 */

function ExportListOfDocuments($log,
		                       $items, 
		                       $local_doc_db_description,
		                       $quick_print, 
		                       &$lo_export) {

	$found_first_doc_of_year = false;
	$page_break = false;
	$is_first_document = true;
	
	// Check if only the items from a certain year are to be exported
	$limit_to_year = CheckAndOutputSingleYearLimit($log, $quick_print, $lo_export);
	
	//For each document
	foreach ($items as $i=>$item) {
			
		// If output of found documents is limited to a certain year		
		if ($limit_to_year != 0) {
			
			if (!IsDocumentNotFromThisYear($item, $limit_to_year, 
				 	                       $found_first_doc_of_year)) {
				continue;
			}			
		}
		
		// See if the user wants a page break after every document
		if (isset($_SESSION['blt_user_pageBreak']) &&
				  $_SESSION['blt_user_pageBreak'] == true){
		
			$page_break = true;		
		}
		
		// But no page break before the first document!
		if ($is_first_document == true) {
			
			$is_first_document = false;
			$page_break = false;
		}		

		ExportDocumentID($item, $lo_export, $page_break, $quick_print, $log);
		
		//Output all fields of the current document
		singleDocLibreOfficeOutput ($lo_export, 
				                    $item, 
				                    $local_doc_db_description, 
				                    true, 
				                    $page_break,
				                    $quick_print);
	
	} // endforeach;
	
}

/**
 * OutputGeneralInformation()
 *
 * Outputs the number of documents found to the Libreoffice Export object
 * 
 */

function OutputGeneralInformation(&$lo_export, $log, $items, $quick_print) {
	
	$str = '<p><strong>' . ucfirst(DatabaseConfigStorage::getCfgParam('doc-name-ui')) . ' List</strong></p>';
	$str .= '<p>' . count($items) . ' entries found.</p><p></p>';
	
	$log->lwrite($str);
	
	$para_format_name = "aa-1";
	if ($quick_print) $para_format_name = "Standard";
	
	$result = $lo_export->Convert_Html_To_LO_XML($str, $para_format_name);

	$log->lwrite($result);
}


/**
 * InsertDocumentID()
 *
 * If the show ID session variable is set, the function inserts HTML code 
 * into the Libreoffice export object to show the document ID.
 * 
 * The document ID is either inserted as text or as a clickable link depending 
 * on PHP session variables.
 * 
 */

function ExportDocumentID ($item, &$lo_export, &$page_break, $quick_print, $log) {

	$str = "";
	
	$para_format_name = "ah-1";
	if ($quick_print) $para_format_name = "Standard";	

	if (isset($_SESSION['blt_user_includeLink']) &&
	$_SESSION['blt_user_includeLink'] == true){
	
		$str = GenerateClickableDocumentIdURL($item);
		
		//$log->lwrite('Clickable doucment ID is: ' . $str);
		$lo_export->Convert_Html_To_LO_XML($str, $para_format_name, $page_break);
		
		// Indicate that the page break was done at this point, no need to
		// make one again later.
		$page_break = false;		
	}
	elseif (isset($_SESSION['blt_user_showId']) &&
	$_SESSION['blt_user_showId'] == true){
			
		// Simple document ID output
		$str = '<p><b>'. ucfirst(DatabaseConfigStorage::getCfgParam('doc-name-ui')) .
		       ' Internal ID&nbsp;: ' . $item->getField('id') .
		       '</b></p>';
		
		$lo_export->Convert_Html_To_LO_XML($str, $para_format_name, $page_break);
		
		// Indicate that the page break was done at this point, no need to
		// make one again later.
		$page_break = false;
	}
	

}
