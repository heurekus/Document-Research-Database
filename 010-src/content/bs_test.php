<?php
/**
 * bs_test.php
 *
 * For experimental stuff, not linked from anywhere.
 *
 * @version    1.0 2017-02-06
 * @package    DRDB
 * @copyright  Copyright (c) 2014-17 Martin Sauter
 * @license    GNU General Public License
 * @since      Since Release 1.0
 */



$log = new Logging();
$log->lwrite('Test page accessed');

echo $_SERVER['REMOTE_ADDR'] . '<br>';


if (UserPermissions::hasAccess('admin')) {    
    $log->lwrite("ACCESS GRANTED");
} else {
    $log->lwrite("ACCESS NOT GRANTED");
    return;
}


// test content
$content = '<p>In-4°, 22 f., [A], A2, A3, [A4], B, B2, B3, [B4], C, C2, C3, [C4], D, D2, D3, [D4], E, E2, F, F2, [F3], [F4], sans pagination, hauteur c. 21,4 cm x largeur c. 14,7 cm.</p><p>&lt;Les&gt; poésies françaises « La Nymphe de France parle. » et « LA NYMPHE ANGEVINE PARLE. » sont insérées à la fin, f. 4, C-C2, D-[D2].</p><p>BM Lyon <span style="background-color:#00ff00;">RES </span><span style="background-color:#ffff00;">32184</span><span style="background-color:#00ff00;">5.</span></p><p><span style="background-color:#00ff00;">LXexemplaire</span> de la BNF Bibl. de lXArsenal 4-BL-2039 (1) ne contient pas les pièces « La Nymphe de France parle. » et « LA NYMPHE ANGEVINE PARLE. », C-C2, D-[D2], hauteur c. 20,4 cm x largeur c. 14,5 cm.</p><p>LXexemplaire de la BM Abbeville FA 16 <span style="background-color:#00ffff;">D 146 conti</span><span style="background-color:#ff0000;">ent les</span><span style="background-color:#00ffff;"> pièces « La N<span style="color:#ee82ee;">ymphe de </span>France parle. » </span>et « LA NYMPHE ANGEVINE PARLE. », C-C2, D-[D2], hauteur c. 20,7 cm x largeur c. 15 cm.</p>';

echo "\n";
echo $content;
$log->lwrite($content);

// add XML encoding as UTF-8 info to string so DOMDocument doesn't mess up characters
$content = '<?xml encoding="utf-8" ?>' . $content;

echo "<br><br>";
echo "\n\n";
$log->lwrite("");

// Use LIBXML for preventing output of doctype, <html>, and <body> tags
$dom = new DOMDocument();
$dom->loadHTML($content, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);

$xpath = new DOMXPath($dom);

foreach ($xpath->query('//span') as $span) {

    // Only remove with color spans
    $style =  $span->getAttribute('style');    
    $log->lwrite($style);
    if (strpos($style, "background-color:") === false and 
        strpos($style, "color:") === false) continue;

    // Move all (color) span tag content to its parent node just before it.
    while ($span->hasChildNodes()) {

        $child = $span->removeChild($span->firstChild);
        $span->parentNode->insertBefore($child, $span);    

    }

    // Remove the span tag
    $span->parentNode->removeChild($span);
}

// Get the final HTML with span tags stripped
$output = $dom->saveHTML();

// Debug: output the raw result
echo $output;
$log->lwrite($output);

// Convert to UTF-8 again, convert special HTML encoded chars to UTF-8 chars
// and remove harmful HTML tags
$output_clean = convertToSafeUtf8Html($output);

// Remove the UTF-8 coding information again that was added above
// --> xml encoding="utf-8" plus brackets and question marks around it!
$output_clean = substr($output_clean, 31);

echo "<br><br>\n\n";
echo $output_clean;
$log->lwrite($output_clean);


return;

?>