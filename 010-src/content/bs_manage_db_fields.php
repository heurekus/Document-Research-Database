<?php
/**
 * bs_manage_db_fields.php
 *
 * GUI frontend for admin users to add, change, delete, rename and 
 * reorder database fields. Many of these actions are done with
 * movable panels for the user to select and change the field order.
 * 
 * Note: Originally bs_change_display_order.php was taken as a template
 * for this page.
 *
 * @version    1.0 2021-05-30
 * @package    DRDB
 * @copyright  Copyright (c) 2014-21 Martin Sauter
 * @license    GNU General Public License
 * @since      Since Release 1.0
 */
?>

<style type="text/css">
    /* show the move cursor as the user moves the mouse over the panel header.*/
    #draggablePanelList .panel-heading {
        cursor: move;
    }
</style>

<script data-cfasync="false" src="/jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>

<!--The following small library modifies jquery-ui to also handle-->
<!--touch events, i.e. panels are movable as well with a touch interface-->
<script src="/jquery.ui.touch/jquery.ui.touch-punch.min.js"></script>

<script src="js/manage_db_fields.js"></script>

<?php 
$log = new Logging();
$log->lwrite('\'Manage db fields\' page accessed');


// If the user has no admin rights, abort
if (!UserPermissions::hasAccess('admin')) {
    return;
}


// Check if a modification opertion is to be run before creating the output
// (e.g. modify/add/delete a field). If there was a problem abort.
if (isset($_POST['operation'])) {
    if (!executePostOperation($log)) return;
}

// Create token if not already done in a previous iteration or somewhere else
// by another action. The token is sent when the save button is pressed
// and then analyzed in createOrModifiyRecord() to prevent Cross Site 
// Request Forgery (CSRF) attacks.
$token = createOrReuseSecurityToken();

// Put document token used by Javascript on the client side into an invisible 
// page element
echo '<span id="doc_token" style="display:none">'. $token .
'</span>' . PHP_EOL;


// Create the bootstrap page layout container, first row for the side
// navigation menu bar.
echo '<div class="container-fluid">' . PHP_EOL;
echo '<div class="row content">' . PHP_EOL;
echo '<div class="col-sm-2 sidenav">'. PHP_EOL;      

// Create the menu area
echo '<br>';
echo '<ul id="menu_area" class="nav nav-pills nav-stacked custom">'. PHP_EOL;

// Create buttons 
echo '<li class="active">';
echo '<a href="#" id="closetab">'. 
'<span class="glyphicon glyphicon-asterisk"></span> Done</a>';
echo '</li>' . PHP_EOL;

echo '<li class="active">';
echo '<a href="#" id="button-create-new-field">'.
    '<span class="glyphicon glyphicon-plus"></span> Create New Field</a>';
echo '</li>' . PHP_EOL;

echo '<li class="active">';
echo '<a href="#" id="button-delete-field">'.
    '<span class="glyphicon glyphicon-trash"></span> Delete A Field</a>';
echo '</li>' . PHP_EOL;


if (UserPermissions::hasAccess('help')) {
    echo '<br>';
    echo '<li class="active">';
    echo '<a href=/drdb-manual/ManageDatabaseFields target="_blank" ';
    echo 'id="menu_text">';
    echo '<span class="glyphicon glyphicon-question-sign"></span> Help</a>';
    echo '</li>';
}

echo '</ul>';

echo '</div> <!-- end of sidenav column --> '; 
echo '<div class="col-sm-4"> <!-- start of middle column -->'; 
echo '<br>';

echo '<div class="alert alert-info" role="alert" id="info-text">';

echo 'Usage:<br>1. Drag/drop the fields into the desired order.<br>' . 
     '2. Double click on field name to rename it.';
echo '</div>';
    
echo '<!-- Bootstrap 3 panel list. -->';
echo '<ul id="draggablePanelList" class="list-unstyled">';
 
// get access to the global $doc_db_description array (read-only)
$doc_db_description = DatabaseConfigStorage::getDbFieldConfig();

// Load the field order.
		
$fieldArrayWithOptions = $doc_db_description;


// Create pannels, one for each field in of the main database table. 
foreach ($fieldArrayWithOptions as $field_print_name=>$field_options):

    // Whether a field is visiable or not is decided with the class
    // parameter panel-primary (shown) vs. panel-warning (not shown).
    // However, for the main database struct all fields are always shown,
    // hence, all panels are 'panel-primary'
   	$classContent = 'panel panel-primary doc-field'; 
   	
   	// Escape double quotes in the field print name, they mess up 
   	// html tags.
   	$field_print_name_for_id = str_replace('"', "&quot;", $field_print_name);

   	echo '<li class="' . $classContent . '" id="' . $field_print_name_for_id . '">';
	echo '<div class="panel-heading">' . $field_print_name;
	echo '<span id="panel-id-print-name" style="display:none">' . 
	     $field_print_name . '</span>';	
	echo '<span id="panel-id-field-name" style="display:none">' . 
	     $field_options[DB_FIELD_NAME] . '</span>';
	echo '<span id="panel-id-f-inline-print" style="display:none">' . 
	     $field_options[PRINT_INLINE] . '</span>';		
	echo '</div>';
	echo '</li>';
	echo PHP_EOL;
endforeach;

?>
   
</ul> <!-- end of panel list -->

    </div> <!-- end of middle column -->        
    <div class="col-sm-6"></div>    
  </div> <!-- end of row -->
</div> <!-- end of container -->


<!-- Modal dialog box for 'Rename Field' functionality -->
<div class="modal fade" id="myRenameModal" role="dialog">
  <div class="modal-dialog">    
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Rename Field</h4>
      </div>
      <div class="modal-body">       
        <form id="rename-field-name-form" action="index.php?content=bs_manage_db_fields" method="post">
          <div class="form-group">
            <input type="hidden" id="id-operation" name="operation" value="change-field-name">          
            <input type="text" class="form-control" id="id-input-field-database-field-name" name="new-field-name" value="">
            <input type="hidden" id="id-original-field-name" name="original-field-name" value="">
            <input type="hidden" id="id-token" name="token" value="<?php echo $token?>">            
            <br>
            <div class="form-check">
              <input type="checkbox" class="form-check-input" id="id-inline-print" name="print-field">
              <label class="form-check-label" for="id-inline-print">Print field inline on export</label>
              <?php insertInlinePrintInfoText() ?>
            </div>
          </div>
        </form>        
      </div>
      <div class="modal-footer">      
        <button type="button" class="btn btn-default" data-dismiss="modal" 
                style="float:left">Abort</button>
        <button type="button" class="btn btn-primary" id="confirm-button-rename-field" data-dismiss="modal" 
                style="float:right">Confirm</button>
      </div>
    </div>    
  </div>
</div>


<!-- Modal dialog box for 'Add Field' functionality -->
<div class="modal fade" id="myAddFieldModal" role="dialog">
  <div class="modal-dialog">    
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add A New Field</h4>
      </div>
      <div class="modal-body">       
        <form id="new-field-name-form" action="index.php?content=bs_manage_db_fields" method="post">
          <div class="form-group">
            <input type="hidden" id="id-operation" name="operation" value="add-field">          
            <input type="text" class="form-control" name="new-field-name" value="">
            <input type="hidden" id="id-token" name="token" value="<?php echo $token?>">
            <br>
            <div class="form-check">
              <input type="checkbox" class="form-check-input" id="id-inline-print" name="print-field">
              <label class="form-check-label" for="id-inline-print">Print field inline on export</label>
              <?php insertInlinePrintInfoText() ?>
            </div>
          </div>
        </form>        
      </div>
      <div class="modal-footer">      
        <button type="button" class="btn btn-default" data-dismiss="modal" 
                style="float:left">Abort</button>
        <button type="button" class="btn btn-primary" id="confirm-button-add-field" data-dismiss="modal" 
                style="float:right">Confirm</button>
      </div>
    </div>    
  </div>
</div>



<!-- Modal dialog box for 'Delete Field' functionality -->
<div class="modal fade" id="myDeleteFieldModal" role="dialog">
  <div class="modal-dialog">    
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Delete A Field</h4>
      </div>
      <div class="modal-body">
      	<p>Type-in the name of the field to delete. CAUTION! All data of this field will be lost!</p>       
        <form id="delete-field-form" action="index.php?content=bs_manage_db_fields" method="post">
          <div class="form-group">
            <input type="hidden" id="id-operation" name="operation" value="delete-field">          
            <input type="text" class="form-control" name="name-of-field-to-delete" value="">
            <input type="hidden" id="id-token" name="token" value="<?php echo $token?>">
          </div>
        </form>        
      </div>
      <div class="modal-footer">      
        <button type="button" class="btn btn-default" data-dismiss="modal" 
                style="float:left">Abort</button>
        <button type="button" class="btn btn-danger" id="delete-field-button" data-dismiss="modal" 
                style="float:right">Delete</button>
      </div>
    </div>    
  </div>
</div>



<?php 


/**
 * insertInlinePrintInfoText()
 *
 * Outputs an info text about the Print Inline checkbox to the DOM
 *
 * @param none
 *
 * @return none
 *
 */

function insertInlinePrintInfoText() {

    echo '<span class="glyphicon glyphicon-info-sign" data-toggle="tooltip" data-placement="left" ' . 
    'data-html="true" title="When records are exported to Libreoffice, the content of a field ' . 
    'is printed in a new line after the field name (box NOT checked). For fields with short text, ' .
    'check the box to print the content of the field next to the field name."></span>';
}

/**
 * executePostOperation()
 *
 * Do a security check with the token and if everything checks out,
 * have a look which operation the user wants to be performed
 * (e.g. modfiy/create/delete a main db table field. Then call
 * the appropriate handler fucntion
 *
 * @param none
 *
 * @return boolean, false if there was an error that should trigger
 *         an abort.
 *
 */

function executePostOperation($log) {
    
    // Run web security checks
    if (checkAntiCsrfProtection() == false) {
        $err_msg = "There was a security error, unable to modify main db field config";
        $log->lwrite($err_msg);
        return false;
    }
    
    switch($_POST['operation']) {
        
        case 'change-field-name':
            $log->lwrite('User wants to change a main table field name');
            MainTableCfg::changeFieldOptions();
            MainTableCfg::changeFieldName();
            break;
            
        case 'add-field':
            $log->lwrite('User wants to add a main table field name');
            MainTableCfg::createNewField();
            break;
            
        case 'delete-field':
            $log->lwrite('User wants to DELETE a main table field');
            MainTableCfg::deleteField();
            break;
            
        default:
            $log->lwrite('ERROR: Unknown POST operation: ' . $_POST['operation']);
    }
    
    return true;
}

?>

