<?php
/**
 * bs_home.php
 *
 * Content for the home page
 *
 * @version    1.2 2017-02-04
 * @package    DRDB
 * @copyright  Copyright (c) 2017 Martin Sauter
 * @license    GNU General Public License
 * @since      Since Release 1.0
 */

$log = new Logging();
$log->lwrite('Main page accessed');

?>

<script src="js/home_init.js"></script>
<script src="js/home_show_hide_searches.js"></script>
<script src="js/home_multi_search.js"></script>

<div class="container-fluid">

  <div class="row content">
    <div class="col-sm-2 sidenav">      
      <br>

      <!-- set 'class' of a list element to "active" to fill! -->
      <ul id="menu_area" class="nav nav-pills nav-stacked custom">
        <li class="active" id="show-options-list">
          <a href="#a1" data-toggle="tab">
          <span class="glyphicon glyphicon-list"></span> List</a>
        </li>
        <li class="active" id="show-search-list">
          <a href="#a2" data-toggle="tab" id="menu_text">
          <span class="glyphicon glyphicon-search"></span> Search</a>
        </li>
        
<?php 
    if (UserPermissions::hasAccess('edit')) {
        
        echo ('
        <li class="active">
          <a href="index.php?content=bs_record_update&id=" id="menu_text" 
             target="_blank"><span class="glyphicon glyphicon-plus-sign">
             </span> Add new</a>            
        </li>
        ');
    }

    if (UserPermissions::hasAccess('admin')) {        
        echo ('
        <br>
        <li class="active">
          <a href="index.php?content=bs_about" target="_blank"
             id="menu_text">
          <span class="glyphicon glyphicon-flag"></span> Administration</a>
        </li>        
        ');
    }        

    if (UserPermissions::hasAccess('help')) {        
        echo ('
            <br>
            <li class="active">
              <a href="#" id="list-search-help"
                 id="menu_text">
              <span class="glyphicon glyphicon-question-sign"></span> Help</a>
            </li>
        ');
     }
?>    
        
      </ul><br>
    </div>
   
   
    <div class="col-sm-10"> 
    

<?php  

// Display a greeting when the page is loaded and fade it out
// after a few seconds (ms-fade triggers a jQuery function in the
// script section above.
$user = $_SERVER['PHP_AUTH_USER'];
echo '<br>';
echo '<div class="alert alert-success ms-fade" role="alert">';
echo '<center><h3>Hello ' . ucfirst($user) . '. Welcome back!</h3></center>';
echo '</div>';
?>
      <!-- List Options -->
      <!-- ############ -->
    
      <div id="options_list" style="display:block">       
         <h2>List <?php echo ucfirst(DatabaseConfigStorage::getCfgParam('doc-name-ui-plural'));?>...</h2><br>
  	     <div class="list-group">
		   <a href="#a2" 
		      class="list-group-item"
		      id="show-list-all-options">
		      <strong>all</strong>, sorted by ID </a>
		    
		   <a href="#a1" 
		      class="list-group-item"
		      id="show-list-by-group">	      
              <strong>of a group</strong> (e.g. a category or decade)</a>   

           <a href="index.php?content=bs_record_list_view&doc_list_type=search_docs_with_colored_text" 
              class="list-group-item" target="_blank">
              with <strong>colored</strong> text</a>
	      </div>        
      </div>
      
      
      <!-- Search Options -->
      <!-- ############## -->
    
      <div id="search_list" style="display:none">    
    
         <h2>Search <?php echo ucfirst(DatabaseConfigStorage::getCfgParam('doc-name-ui'));?>...</h2><br>
  	     <div class="list-group">
		      		    
		   <a href="#none" 
		      class="list-group-item"
		      id="show-multi-search">
		      for <strong>different</strong> texts in <strong>several</strong> database fields
		   </a>
		      
		   <a href="#none" 
		      class="list-group-item"
		      id="show-search-text-all-fields">
		      by <strong>text in all fields</strong>
		   </a>  
		   
<?php
if (UserPermissions::hasAccess('edit')) {
    echo ('
      <a href="#none"
        class="list-group-item"
        id="show-search-replace-all-fields">
        and <strong>replace text in all fields</strong>
      </a>
     ');    
}
?>		   

		   <a href="#none" 
		      class="list-group-item"
		      id="show-empty-field-search">
		      with a field that is <strong>empty</strong>
		   </a>
              
	      </div> <!-- list group -->        
      </div> <!-- search list -->    
           
      <!-- Search in all fields -->
      <!-- ==================== -->

      <div id="search_text_all_fields" style="display:none">
      <form action="index.php?content=bs_record_list_view&doc_list_type=search_in_all_fields" 
            method="post" name="doc_list_view" id="doc_list_view" 
            target="_blank">
            <fieldset class="searchform">  
               <h2>Search Text in All Fields</h2>
               <div class="well">
                 <p><strong>Search Text</strong></p>
                 <input class="form-control" id="Text" name="Text" type="text">
               </div> <!--  end of well -->                        
                                                   
               <?php
               insertCaseSensitiveCheckBox();
               insertCategorySelection('Limit Search to Decade/Category');               
               insertShowExtraFieldsSelection();
               ?>                 
               
               <br>
               <input class="btn btn-primary" type='submit' value='Search' />
             </fieldset>
      </form>      
      </div>

      <!-- Search/Replace in all fields-->
      <!-- =========================== -->

      <div id="search_replace_all_fields" style="display:none">
      <form action="index.php?content=bs_record_list_view&doc_list_type=search_in_all_fields" 
            method="post" name="doc_list_view" id="doc_list_view" 
            target="_blank">
            <fieldset class="searchform">  
               <h2>Search/Replace for Text in All Fields</h2>

               <br>
               <div class="alert alert-info">
                 <h1><div class="glyphicon glyphicon-info-sign"></div></h1>
                 <div>
                   <ul>
                    <li>By default the search is case IN-sensitive. Tick the corresponding checkbox to make the search case sensitive! 
                    <li>After the search the result will be presented.
                    <li>In addition, changed fields will be shown how they WOULD look like after the change. 
                    <li>Changes are only performed after pressing the "Commit" button!
                   </ul>
                 </div>                 
               </div>                                          
               
               <div class="well">
                 <p><strong>Search Text:</strong></p>
                 <input class="form-control" id="Text" name="Text" type="text">
               </div>

               <?php
               insertCaseSensitiveCheckBox();               
               ?>
               
               <div class="well">
                 <p><strong>Replacement Text:</strong></p>
                 <input class="form-control" id="Replacement_Text" name="Replacement_Text" type="text">
               </div>
               
			   <div class="well">
			     <p><strong>Optional: Modify Style</strong></p>
			     <select name="style_change">
			       <option value="-">-</option>
			       <option value="add_italics">Add italics</option>
			       <option value="remove_italics">Remove italics</option>
			       <option value="add_bold">Add bold</option>
			       <option value="remove_bold">Remove bold</option>
			     </select>
			   </div>               
               
               <?php 
               insertCategorySelection('Limit Search to Decade/Category');              
               ?>  
               
               <br>               
               <br>
               <input class="btn btn-primary" type='submit' value='Search' />
             </fieldset>
      </form>      
      </div>      
      
      <!-- List All -->
      <!-- ================================ -->

      <div id="list_all" style="display:none">

<?php 

$doc_search_type = "";
echo '<h2>' . ucfirst(DatabaseConfigStorage::getCfgParam('doc-name-ui')). ' List All</h2>';

echo '<form action="index.php?content=bs_record_list_view&' . 
       'doc_list_type=' . $doc_search_type . 
       '" method="post" name="doc_list_view" ' . 
       'id="doc_list_view" target="_blank">'; 
echo PHP_EOL;	
insertShowExtraFieldsSelection();
echo PHP_EOL;
echo "<input type='submit' value='Search' />";
echo "</form>";
      
?>           
      </div> <!-- end of list_by_group -->       
      
      <!-- List/Search for a group (decade) -->
      <!-- ================================ -->

      <div id="list_by_group" style="display:none">

<?php 

$doc_search_type = "search_docs_category";

echo '<h2>' . ucfirst(DatabaseConfigStorage::getCfgParam('doc-name-ui')). ' List by Category/Decade</h2>';

echo '<form action="index.php?content=bs_record_list_view&' . 
       'doc_list_type=' . $doc_search_type . 
       '" method="post" name="doc_list_view" ' . 
       'id="doc_list_view" target="_blank">'; 

insertCategorySelection('Limit Search to Decade/Category');

echo PHP_EOL;	

echo '<div class="well">';
echo '<p><strong>Optional: Limit to a certain year</strong></p>';
echo '<input class="form-control" id="limit_to_year" name="limit_to_year" ' . 
     'type="limit_to_year">';
echo '</div>';

insertShowExtraFieldsSelection();

echo PHP_EOL;

echo "<input type='submit' value='Search' />";

echo "</form>";
      
?>           
      </div> <!-- end of list_by_group -->       

      
      
      <!-- Multi-Search                     -->
      <!-- ================================ -->

      <div id="multi_search" style="display:none">
      
<?php


$local_doc_db_description = DatabaseConfigStorage::getDbFieldConfig();

define ('NUM_OF_INPUT_FIELDS', 7);

$doc_search_type = "search_docs_multi_select";

echo '<h2>' . ucfirst(DatabaseConfigStorage::getCfgParam('doc-name-ui')) . ' Multi-Search</h2>';

echo '<div class="alert alert-info">';
echo '<h1><div class="glyphicon glyphicon-info-sign"></div></h1>';
echo '  <div>';
echo '  <ul>';
echo '   <li>Select <b>"NOT like"</b> if the field shall not contain the search term.</li>';
echo '   <li>Select <b>"EMPTY"</b> to require that the field is empty.</li>';
echo '   <li>Select <b>"NOT like"</b> and <b>"EMPTY"</b> to require that the field is NOT empty.</li>';
echo '   <li>Use the <b>"+"</b> button to get an additional search term field.</li>'; 
echo '   <li>Search terms can be combined with <b>AND</b> or <b>OR</b>!</li>';
echo '  </ul>';
echo '  </div>';
echo '</div>';

echo PHP_EOL;
echo PHP_EOL;

echo '<form id="multi-select-form" target="_blank" action="index.php?content=bs_record_list_view&' . 
       'doc_list_type=' . $doc_search_type . '" ' .
       'method="post" name="doc_list_view" id="doc_list_view">'; 

echo '<fieldset class="searchform">';

for ($i = 0; $i < NUM_OF_INPUT_FIELDS; $i++) {
	
	// make only the first search term visible
	if ($i == 0){
		echo '<div id="id_search_term_' . $i . '">';	
	} else {
		echo '<div id="id_search_term_' . $i . '" style="display: none">';		
	}

	echo '<div class="well">';
		
	echo '<select name="db_multi_field_' . $i . '" class="class_db_multi_field">';
	echo '<option value="-">-</option>';
	// put all possible database fields in the drop down list
	foreach ($local_doc_db_description as $field_print_name=>$field_options):
		echo '<option value="' . $field_options[DB_FIELD_NAME] . '">' . 
	         $field_print_name . '</option>';
		echo PHP_EOL;
		endforeach;
	
	echo '</select>';

	// Checkbox for a "NOT LIKE" search in that field
	echo '&nbsp;';
	echo '<div class="checkbox-inline">';
	echo '<label><input name="not_like_' . $i . '" ' .
		 'class="class_not_like_' . $i . '" ' . 
         'type="checkbox" value="">NOT like</label>';
	echo '</div>';

	// Checkbox for a "EMPTY"
	echo '&nbsp;';
	echo '<div class="checkbox-inline">';
	echo '<label><input name="empty_' . $i . '" ' .
		 'class="class_empty_' . $i . '" ' .
		 'type="checkbox" value="">EMPTY</label>';
	echo '</div>';
	
	// The text string to search for
	echo '<input class="form-control class_db_field_string_'. $i . '" ' . 
	      'name="db_field_string_' . $i . '" ' .
	      'id="db_field_string_' . $i . '">';

	
	// Put a "+" button at the end of the search term well so the user
	// can make another search term well visible. No button is created for
	// at the last search term well!
	if ($i != NUM_OF_INPUT_FIELDS - 1) {
		echo '<div id="id_add_search_term_button_' . $i. '">' .
			 '<p><br><button type="button" class="btn-xs btn-info pull-right add-term">' .			 
			 '<span class="glyphicon glyphicon-plus">' .
			 '</span></button></p></div>';		
	}
	
	echo '</div>'; // end of the well
	echo PHP_EOL;

	echo '</div>'; // end of div for hide/show of a search term well
}
 
// Add bool combine radio buttons. They are invisible at first and made
// visible as soon as the user adds another search term.
echo '<div class="well" style="display: none" id="id_bool_combine">';
echo '<div class="radio">';
echo '<label><input type="radio" name="bool-combine" value="and" checked="checked"><strong>AND combine</strong></label>';
echo '</div>';
echo '<div class="radio">';
echo '<label><input type="radio" name="bool-combine" value="or"><strong>OR combine</strong></label>';
echo '</div>';
echo '</div>'; //end of well

insertCaseSensitiveCheckBox();
insertCategorySelection('Limit Search to Decade/Category');
insertShowExtraFieldsSelection();


echo "    <input class='btn btn-primary' type='submit' value='Search' />";
echo "</fieldset>";
echo "</form>";

?>      
      
      
      </div> <!-- multi-search -->      
      
      
      <!-- Search for empty field           -->
      <!-- ================================ -->

      <div id="emtpy_field_search" style="display:none">      
      
<?php

echo '<h2>' . ucfirst(DatabaseConfigStorage::getCfgParam('doc-name-ui')) .' Search for Empty Fields</h2>';

echo '<div class="alert alert-info">';
echo '<h1><div class="glyphicon glyphicon-info-sign"></div></h1>';
echo '<div>Searches all ' . DatabaseConfigStorage::getCfgParam('doc-name-ui') . ' records in which the ';
echo 'selected field is empty. This can be used, for example, to find ';
echo 'all ' . DatabaseConfigStorage::getCfgParam('doc-name-ui') . ' records that DO NOT have a ';
echo 'libretto editor.</div>';
echo '</div>';

echo '<form target="_blank" action="index.php?content=bs_record_list_view&' . 
     'doc_list_type=search_docs_empty_field' . 
     '" method="post" name="doc_list_view" id="doc_list_view">'; 

echo '<div class="well">';
echo '<p><strong>Select Field</strong></p>';

echo '<fieldset class="searchform">';
echo '<select name="db_field_empty">';

// put all possible database fields in the drop down list
foreach ($local_doc_db_description as $field_print_name=>$field_options):
	echo '<option value="' . $field_options[DB_FIELD_NAME] . '">' . 
         $field_print_name . '</option>';
	echo PHP_EOL;
	endforeach;

echo '</select>';
echo '</fieldset>';

echo '</div>'; //end of well

insertCategorySelection('Limit Search to Decade/Category');
insertShowExtraFieldsSelection();

echo PHP_EOL;	
   
echo "    <input class='btn btn-primary' type='submit' value='Search' />";

echo "</form>";

?>      
      
      </div> <!--  Search for empty field -->
      
      <br>  
    </div> <!-- end of main column content-->
  </div> <!-- end of 1st row conent -->
</div> <!--  end of the overall container" -->   


<?php


//
//
// insertCaseSensitiveCheckBox()
//
//
function insertCaseSensitiveCheckBox() {
	
	echo '<div class="well">';

	echo '<div class="checkbox">';
	echo '<label><input class="case_sensitive" type="checkbox" name="case_sensitive" value="">';
	echo 'Case Sensitive Search</label>';
	echo '</div>';
	
	echo '</div>'; //end of well
	
}

function insertShowExtraFieldsSelection() {

	echo '<div class="well">';
	echo '<p><strong>Optional: In the result list show extra fields ' . 
	     'which are not part of the search</strong></p>';

	// Show a checkbox with which the user can select to show extra fields	
	echo '<table><tr><td>';
	echo '<div class="checkbox">';
	echo '<label><input class="extra_fields" type="checkbox" ';
	echo 'name="extra_fields" value="">'; 
	echo 'Show extra fields&nbsp;&nbsp;&nbsp</label>';
	echo '</div>';	
	
	echo '</td><td>';
	
	echo '<a href="index.php?content=bs_change_display_order&id=0" ';
	echo 'class="btn btn-primary check-extra-fields" role="button" target="_blank">';  
	echo 'Select Fields</a>';
	
	echo '</td></tr></table>';
	
	echo '</div>'; //end of well
	
}

?>

<!-- Modal dialog box for reporting a user input error -->
<div class="modal fade" id="multi_search_validation_error_box" role="dialog">
  <div class="modal-dialog">
    
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Something didn't check out!</h4>
      </div>
      <div class="modal-body">
        <p id="multi_search_error_text">x</p>
      </div>
      <div class="modal-footer">                  
        <button type="button" class="btn btn-default" data-dismiss="modal" 
                style="float:right">OK</button>                      
      </div>
    </div>
    
  </div>
</div>

<!-- end content -->