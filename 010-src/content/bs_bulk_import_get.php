<?php
/**
 * bs_bulk_import_get.php
 *
 * TBD
 *
 * @version    1.0 2018-08-06
 * @package    DRDB
 * @copyright  Copyright (c) 2014-18 Martin Sauter
 * @license    GNU General Public License
 * @since      Since Release 1.0
 */

$log = new Logging();
$log->lwrite('bs_bulk_import_get.php accessed');

// create token if not already done in a previous iteration or somewhere else
// by another action. The token is sent when the "save" button is pressed
// and then analyzed in the createOrModifiyRecord() function in 
// create-or-modify-record.ph to prevent Cross Site Request Forgery (CSRF) 
// attacks.
$token = createOrReuseSecurityToken();

?>

<center>

<h2>Bulk Data Import</h2>
<br>
Note: The maximum file size is <?php echo ini_get('upload_max_filesize'); ?>bytes.
<br>
Depending on the amount of data, the import process can take <b>several minutes!</b> 
<br>
<b>Remain on the page</b> until a success or error message is shown!
<br>
<br>

<form action="index.php?content=bs_bulk_import_process" method="post" enctype="multipart/form-data">
    <input type="file" name="fileToUpload" id="fileToUpload"><br>
    <input type="hidden" name="token" value="<?php echo $token; ?>">
    <input type="submit" value="Upload file" name="submit">
</form>

</center><br>';

<?php 
return;
?>