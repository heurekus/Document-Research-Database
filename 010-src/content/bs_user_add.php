<?php
/**
 * bs_user_add.php
 *
 * GUI for adding a new user
 *
 * @version    1.0 2021-04-26
 * @package    DRDB
 * @copyright  Copyright (c) 2014-21 Martin Sauter
 * @license    GNU General Public License
 * @since      Since Release 1.0
 */

$log = new Logging();
$log->lwrite('User addition page accessed');


if (!UserPermissions::hasAccess('admin')) {
    $log->lwrite("User has no admin rights, aborting");
    return;
}

// Create token if not already done in a previous iteration or somewhere else
// by another action. The token is sent when the save button is pressed
// and then analyzed in createOrModifiyRecord() to prevent Cross Site
// Request Forgery (CSRF) attacks.
$token = createOrReuseSecurityToken();

?>
<script src="js/user_management.js"></script>

<div class="container-fluid">

  <div class="row content">
    <div class="col-sm-2 sidenav">      
      <br>

      <ul id="menu_area" class="nav nav-pills nav-stacked custom">
        <li class="active" id="closetab">
          <a href="#a1" id="menu_text">
             <span class="glyphicon glyphicon-asterisk"></span> Close tab</a><br>
        </li>       
       </ul>
    </div>
   
    <div class="col-sm-10"> 

<br>
<h2>Add a New User</h2>
<br>


<?php 


echo '<div class="panel-group">';
echo '<div class="panel panel-info">';     
echo '<div class="panel-heading">';

echo '<strong> ';
echo 'Details';
echo '</strong>';

echo '</div>'; // end of pannel-heading


echo '<div class="panel-body">';
echo '<div class="form-check">';

// Checkbox for each type of permission
echo '<div class="row">';
echo PHP_EOL;

// Create a form for the checkboxes and the update button to submit
// changes back to the server. the updated goes to bs_user_management.php 
// and NOT to this source file!
echo PHP_EOL . PHP_EOL . PHP_EOL;
echo '<form action="index.php?content=bs_user_management" method="post">' . PHP_EOL;
echo '<input type="hidden" id="id-token" name="token" value="' .  $token . '">';

// Include "operation=add_user" as a hidden element in the form to instruct
// the backend to add a new user.
echo '<input type="hidden" name="operation" value="add_user">' . PHP_EOL;


// Include input boxes for the username and password
?>

<div class="form-group">
  <label for="usr">Name:</label>
  <input type="text" class="form-control" name="user">
</div>
<div class="form-group">
  <label for="pwd">Password:</label>
  <input type="text" class="form-control" name="password">
</div>

<?php 

$permission_categories = UserPermissions::getPermissionCategories();
foreach ($permission_categories as $permission_category):
    //$log->lwrite("Permission: " . $permission_category);   
    echo '<div class="col-sm-2">' . PHP_EOL;
    
    // add below for 'input' depending on permission:
    // checked="false", disabled="false" (--> can't be changed)

    echo '<input type="checkbox" class="form-check-input" name="'  
         .$permission_category . '">';
    
    echo PHP_EOL . '<label class="form-check-label" for="exampleCheck1">&nbsp;' . 
         $permission_category . ' </label>';

    echo PHP_EOL . "</div> <!--  end of of column -->" . PHP_EOL . PHP_EOL;

endforeach;


echo '<div class="col-sm-2">'; // column for the buttons

// HTML code for the 'Create' button
echo PHP_EOL;
echo '<button type="submit" class="btn btn-success update-user">' .
    '<span class="glyphicon glyphicon-ok"></span> ' .
    'Create&nbsp;&nbsp;&nbsp;&nbsp;</button><br><br>';
echo PHP_EOL;

echo '</div>'; // end of column for the buttons

echo PHP_EOL;
echo "</form>";
echo PHP_EOL;

echo PHP_EOL;
echo '</div> <!-- end of form permissions form -->';
echo PHP_EOL;
echo '</div> <!-- end of row -->';

echo PHP_EOL;
echo '</div>'; //panel-body
echo '</div>'; //end of pannel
echo '</div>'; //end of pannel group
echo PHP_EOL;

echo '<br>';



?>
</div> <!-- end of right column -->
</div> <!-- end of row -->
</div> <!-- end of container -->
