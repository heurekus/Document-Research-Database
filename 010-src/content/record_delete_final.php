<?php
/**
 * record_delete_final.php
 *
 * This page triggers the real deletion of a database record after
 * doc_delete.php has verified that the user really wants to delete the
 * entry
 *
 * @version    1.0 2017 03 12
 * @package    DRDB
 * @copyright  Copyright (c) 2017 Martin sauter
 * @license    GNU General Public License
 * @since      Since Release 1.0
 */

$id = (int) $_GET['id'];

$log = new Logging();

if ($id) {

	// Web security check
	if (checkAntiCsrfProtection() == false) {
		echo '<p id="doc-del-result">There was a security error, ' .
		     'deletion aborted!</p>';
		exit;
	}	

	if (!UserPermissions::hasAccess('edit')) {
	    echo '<p id="doc-del-result">ERROR: No edit rights, record not deleted!</p>';
	    $log->lwrite("User has no 'edit' rights, deleting aborted!");
	    return;
	}
	
	// Delete the main database record for the document
	$result = DocRecord::deleteDoc($id);
 	
 	if ($result == TRUE){
 		echo '<p id="doc-del-result">' . ucfirst(DatabaseConfigStorage::getCfgParam('doc-name-ui')) . 
 		     ' successfully deleted!</p>';
 		
 		$log->lwrite('document ' . $id . ' successfully DELETED!');
 	}
 	else {
 		echo '<p id="doc-del-result">Database error while deleting document!</p>';
 		
 		$log->lwrite('Error 1 deleting document');
 		
 		exit;
 	}
 	
 	// Delete the modification history of all fields
 	$result_mod_history = ModHistoryStorage::deleteRecords($id);
 	
 	if ($result_mod_history == TRUE){
 		echo '<p id="mod-history-del-result">Modification History successfully deleted!</p>';
 			
 		$log->lwrite('Modification history records with id ' . $id . ' successfully DELETED!');
 	}
 	else {
 		echo '<p id="doc-del-result">Database error while deleting document!</p>';
 			
 		$log->lwrite('Error deleting modification history');
 			
 	}
 	
}
else {
    echo '<p id="doc-del-result">Error deleting document! ID ' . 
         'was not present in request</p>';

    $log->lwrite('Error 2 deleting document, id was not present in request');
}

?>






