<?php
/**
 * unlock_doc_record.php
 *
 * This page UN-locks a database record. The page is called when the user
 * leaves the details or update record page but has not saved his data. 
 * It is then checked if the database record is locked and unlocks it so
 * other people can modify the record despite the user not having saved
 * his data and released the lock properly.
 * 
 * The unlock function is informed that no data was written which prevents
 * the last update time in the record from being updated.
 * 
 * Note: The page returns success or failure information but in the case of
 * the user closing the details/update page it is ignored.
 *
 * @version    1.0 2017 03 18
 * @package    DRDB
 * @copyright  Copyright (c) 2017 Martin sauter
 * @license    GNU General Public License
 * @since      Since Release 1.0
 */

$log = new Logging();

$id = (int) $_GET['id'];

if (!$id) {
	
	echo '<p id="doc-lock-result">Error unlocking document! ID ' .
			'was not present in request</p>';
	$log->lwrite('unlock_doc_record.php: Error unlocking document, ' . 
			     'id was not present in request');
	
} else {

	// Web security check
	if (checkAntiCsrfProtection() == false) {
				
		echo '<p id="doc-unlock-result">There was a security error, ' .
		     'un-locking aborted!</p>';				
		$log->lwrite('unlock_doc_record.php: Security error, ' . 
				     'un-locking aborted ');
		exit;
	}

	// Un-lock the database record
	if ($id != 0){
		
		$log->lwrite('unlock_doc_record.php: Attempting to UN-lock record');
		
		$item = DocRecord::getRecord($id);
		
		$lock_result = isRecordStillLockedForThisUser($id);
	
		if ($lock_result[0] != true){
					
			$res_text = '<p id="doc-unlock-result">' . 
			            'Document was not locked!' . 
			            '</p>';
			            
			echo $res_text;		
			$log->lwrite('unlock_doc_record.php: document was not locked');

		}
		else {
			
			// Un-lock the database record, 2nd param is false, i.e.don't 
			// update the 'last update' time, because nothing was written.
			unlockDatabaseRecord($id, false);
			
			echo '<p id="doc-lock-result">Document successfully UN-LOCKED!</p>';
			$log->lwrite('unlock_doc_record.php: document ' . $id . 
					     ' successfully UN-LOCKED!');			
		}
	}
	 	
}

?>
