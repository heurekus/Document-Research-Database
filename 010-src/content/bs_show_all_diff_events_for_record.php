<?php
/**
 * bs_show_all_diff_events_for_record.php
 *
 * @version    1.0 2018-07-06
 * @package    DRDB
 * @copyright  Copyright (c) 2014-18 Martin Sauter
 * @license    GNU General Public License
 * @since      Since Release 1.0
*/

$log = new Logging();

$log->lwrite('bs_show_all_diff_events_for_record: output all diffs');

if (!UserPermissions::hasAccess('history')) {
    $log->lwrite("User has no 'history' permission, aborting");
    return;
}

if (!isset($_GET['id'])) {
	echo '<h1> ERROR: Parameter "id" missing! </h1>';
	$log->lwrite('bs_diff_view.php: Error, "id" parameter missing');
	return;
}
$id = (int) $_GET['id'];

?>

<script src="js/show_all_diff_events.js"></script>

<div class="container-fluid">


  <div class="row content">
    <div class="col-sm-2 sidenav">      
      <br>
      <ul id="menu_area" class="nav nav-pills nav-stacked custom">
        <li class="active">
          <a href="#a1" id="closetab">
          <span class="glyphicon glyphicon-asterisk"></span> Close tab</a>
        </li>
        <br>
      </ul>
    </div>
   
    <!-- end of menu side panel, start with the right panel -->
       
    <div class="col-sm-10">

<?php 

echo "<br>";
echo PHP_EOL;

$mod_history_records = ModHistoryStorage::getAllDiffsForId($id);

if ($mod_history_records === false) {
	echo '<br>error retrieving records';
	return;
}

if (count($mod_history_records) < 1) {
	echo '<br>no mod history records for that id';
	return;
}


$local_doc_db_description = DatabaseConfigStorage::getDbFieldConfig();

// The result array contains the database field name which needs
// to be translated into the GUI name of the field. Both are contained
// in the $doc_db_description associative array:
//
//  * The GUI field name as the key of the associative array
//  * The database field name as the first sub-array entry.
//
$gui_field_names = array_keys($local_doc_db_description);
$db_field_names = array_column($local_doc_db_description, 0);

// Output the header of the page with the first field of the record which
// is used to identify the record
$item = DocRecord::getRecord($id);
$first_field_str = $item->getField($db_field_names[0]);
$first_field_str= prepareFieldTextForOutput($first_field_str);

echo '<div class="panel-group">';
echo '<div class="panel panel-info">';
echo '<div class="panel-heading">';
echo '<p><strong>List of Changes Made To Document Record:</strong>' . $first_field_str .
     '</p>';
echo '</div>'; // panel-heading

echo '</div>'; // end of pannel
echo '</div>'; // end of pannel group

foreach($mod_history_records as $record){

	echo '<br>';
	
	echo '<div class="panel-group">';
	echo '<div class="panel panel-info">';
	echo '<div class="panel-heading">';
	
	echo '<p>' . date('Y-m-d H:i:s', $record['mod_time']) . ', ' . 
	     ucfirst($record['mod_user']) . '</p>'; 

	echo PHP_EOL;
	
	echo '  </div>'; //end of panel-heading
	
	echo PHP_EOL;
	
	echo '<div class="panel-body">';

	echo '<p>Field modified: ';
	// Translate the db_field name into the GUI field name by
	// searching at which position the db_filed name is in the
	// db_field_names array and then using this index to
	// the the GUI name from the gui_field_names array.
	$i = array_search($record['field_name'], $db_field_names);
	if ($i !== false) {
		echo '<a href="index.php?content=bs_diff_view&id=' . $id . 
		     '&field_name=' .$record['field_name'] . 
		     '" target="_blank">' . $gui_field_names[$i] . '</a>';
	}
	else {
		echo '(internal) id';
	}
	echo '</p>';	

	echo '</div>'; // panel-body
	echo '</div>'; // end of pannel
	echo '</div>'; // end of pannel group
	
	echo PHP_EOL;
	

}
?>

    </div> <!-- end of right column -->
  </div> <!-- end of row -->
</div> <!-- end of container --> 
