<?php
/**
 * record_details_print.php
 *
 * Generate a LibreOffice document of the given record id
 *
 * @version    1.0 2019 01 26
 * @package    DRDB
 * @copyright  Copyright (c) 2019 Martin Sauter
 * @license    GNU General Public License
 * @since      Since Release 1.0
 */

require_once 'x-3rd-party-includes/libreoffice-export/lo-writer-export.php';

$local_doc_db_description = DatabaseConfigStorage::getDbFieldConfig();

$log = new Logging();

if (!UserPermissions::hasAccess('export')) {
    $log->lwrite('User has no export rights, aborting');
    return;
}

$id = (int) $_GET['id'];

// Load the document into an object
if ($id) {
  	// Get the existing information for an existing item
 	$item = DocRecord::getRecord($id);
}
else {
    // Get the last item from the database (which was newly added)
    $item = DocRecord::getRecord(0); 
}

$log->lwrite('Document details export to LibreOffice for id ' . $item->getId());

// Check if the user wants output without fancy formatting
if (isset($_GET['quick_print'])) {
    $quick_print = true;
}
else {
    $quick_print = false;
}

// Create a writer export object to generate XML formatted text from
// a database record and to hold style information.
$lo_export = new LO_Writer_Export();

// Output a clickable URL to the document.
$para_format_name = "ah-1";
if ($quick_print) $para_format_name = "Standard";
$str_doc_id = GenerateClickableDocumentIdURL($item);
$lo_export->Convert_Html_To_LO_XML($str_doc_id, $para_format_name);

// Output fields of the current database record
singleDocLibreOfficeOutput ($lo_export,
                            $item, 
		                    $local_doc_db_description, 		            			      
		                    false, false, $quick_print);	    

SendLibreofficeWriterDocOverHttp ($log, $lo_export);

?>

