<?php
/**
 * bs_lo_writer_template_up_handle.php
 *
 * This page is called when a Libreoffice template document was uploaded.
 * It does some consistency checks and then replaces the existing template
 * document.
 *
 * @version    1.0 2019-03-18
 * @package    DRDB
 * @copyright  Copyright (c) 2014-19 Martin Sauter
 * @license    GNU General Public License
 * @since      Since Release 1.0
 */

require_once 'includes/file-upload.php';

$log = new Logging();
$log->lwrite('Libreoffice template document upload handling page accessed');

if (!UserPermissions::hasAccess('export')) {
    $log->lwrite('User has no export rights, aborting template upload');
    return;
}

$result = drdb_handle_file_upload ('fileToUpload', 'templates', 
                                   'writer-export-template.odt', 50000000);

if ($result !== true) {

    echo '<br><center><h3>Error:' . $result . '</h3></center><br>';
    return;    
}

echo '<br><center><h3>Template file upload successful!</h3></center><br>';
return;  
    
?>