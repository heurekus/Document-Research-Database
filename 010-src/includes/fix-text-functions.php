<?php
/**
 * fix-text-functions.php
 * 
 * This file contains a number of functions that are needed in functions.php
 * to modify input text to adhere to the French way of setting quotation
 * marks and spaces before certain punctuation marks. This has to be done as
 * non-French keyboards.
 *
 *
 * @version    1.0 2017-05-21
 * @package    DRDB
 * @copyright  Copyright (c) 2014-17 Martin Sauter
 * @license    GNU General Public License
 * @since      Since Release 1.0
 */


/**
 * fixFrenchPunctuationAndHtmlLinks
 *
 * This function calls a number of different functions in this file
 * to fix French punctuation and HTML links. For details see the
 * description of the different functions.
 * 
 * If 'fix_french_punctuation' in config.php is not set to TRUE the
 * function will exit straight away and return the input without changing it.
 *
 * @param $input_string
 *
 * @return $output_string with punctuation and HTML links fixed
 */

function fixFrenchTextHtmlLinks($input_str) {
	
	if (DatabaseConfigStorage::getCfgParam('fix_french_punctuation') != true) {
		return($input_str);
	}
	
	$input_str = convertNeutralToFrenchQuotes($input_str);
	
	$input_str = fixFrenchPunctuation($input_str);
	
	$input_str = fixFrenchPunctuation2($input_str);
	
	$input_str = fixHttpAndHttpsReferers($input_str);
	
	$input_str = fixApostrophe($input_str);
	
	return($input_str);
}

/**
 * convertNeutralToFrenchQuotes
 *
 * Replace neutral quotes with French left and right quotes 
 *
 * @param $input_string
 * 
 * @return $output_string with changed quotations
 */
function convertNeutralToFrenchQuotes($input_str)
{	
	$log = new Logging();
	
	// Replace neutral quotes with French left and right quotes
	$left_right = 0;
	do {
		$quote_loc = strpos($input_str, "&quot;", 0);

		// Alternate between French left and right quotes
		if ($quote_loc !== false) {
			if ($left_right % 2 > 0){
				$input_str = substr_replace($input_str,
						"&nbsp;&raquo;", $quote_loc, 6);
			}
			else {
				$input_str = substr_replace($input_str,
						"&laquo;&nbsp;", $quote_loc, 6);

			}
			$left_right++;
			$log = new Logging();
			$log->lwrite('AUTO-FIX 1: !!! EXCHANGED !!! neutral quotes to French style');
			$log->lwrite('AUTO-FIX 1 Loc: '. $quote_loc . ' Content: ' . 
					     $input_str);
		} // exchange was made
		
		// Finally, check if there are white spaces after quotes + 
		// non-break spaces and remove them.
		
		$count = 0;
		$input_str = str_replace (" &nbsp;&raquo;", "&nbsp;&raquo;", 
				                  $input_str, $count);
		if ($count > 0) {
			$log->lwrite("AUTO-FIX 1: !!! REMOVED !!! right whitespace " . $count . 
					     " times");
		}

		$count = 0;
		$input_str = str_replace ("&laquo;&nbsp; ", "&laquo;&nbsp;",
				$input_str, $count);
		if ($count > 0) {
			$log->lwrite("AUTO-FIX 1: !!! REMOVED !!! left whitespace " . $count .
					" times");
		}		
		

	} while ($quote_loc !== false); // End of French quote conversion
	
	return ($input_str);
}

/**
 * fixFrenchPunctuation
 *
 * Replaces space for a non-breakable space (&nbsp) in front of the following
 * punctuation characters:
 * 
 * ?, !, :, ;
 * 
 * In addition it replaces lower-start + upper-end quotes into French
 * quotes and non-breakable spaces.
 *
 * @param $input_string
 *
 * @return $output_string with changed quotations
 */

function fixFrenchPunctuation($input_str){
	
	$log = new Logging();
	
	//$log->lwrite("AUTO-FIX 1 - Input: " . $input_str);
	
	$count = 0;
	$spaceAndChar = array (" ?" => "&nbsp;?", 
			               " !" => "&nbsp;!", 
			               " :" => "&nbsp;:",
						   " ;" => "&nbsp;;", 
						   "&bdquo;" => "&laquo;&nbsp;",
						   "&ldquo;" => "&nbsp;&raquo;"
			              );
	
	foreach ($spaceAndChar as $search_str=>$replace_str):
	    $input_str = str_replace ($search_str, $replace_str, $input_str, $count);
		if ($count > 0) {
			$log->lwrite('AUTO-FIX 2: !!! INSERTED !!!  "' . $search_str . '" ' . 
					     $count . ' times'); 
		}			
	endforeach;

	//$log->lwrite("AUTO-FIX 1 - Outpt: " . $input_str);
	
	return($input_str);
}

/**
 * fixFrenchPunctuation2
 *
 * This French punctuation fix function should be called after 
 * fixFrenchPunctuation() as it continues the work. It will find the 
 * following special characters WITHOUT a preceeding space
 * character and add a non-breaking space in front of them:
 * 
 * ?, !, :, ;
 * 
 * The function does not insert non-breaking spaces when those characters
 * are used in HTML markup (< >) or HTML characters (&xxxx;)
 *
 * @param $input_string
 *
 * @return $output_string with changed quotations
 */

function fixFrenchPunctuation2($input_str){

	//return($input_str);

	$i = 0;
	$log = new Logging();
	
	if (strlen($input_str) < 1) {
		//$log->lwrite('AUTO-FIX 3: String too short, exiting function');
		return ($input_str);
	}
	
	do {
		// If the current character is the start of an HTML tag
		if ($input_str[$i] == "<") {
			// Jump to the end of tag or end of string
			//$log->lwrite('AUTO-FIX 3: Jumping over HTML tag at location ' . $i);
			do {
				$i++;
			} while (strlen($input_str) > $i && $input_str[$i] != ">");
			
			// Go to the next character and go straight to the end of the
			// loop so the next character is NOT analyzed below
			$i++;
			continue;
		}
		
		// If the current character is the start of an HTML encoded character
		if ($input_str[$i] == "&") {
			// Jump to the end of the HTML encodec char
			//$log->lwrite('AUTO-FIX 3: Jumping over HTML encoded char at loc ' . $i);
			do {
				$i++;
			} while (strlen($input_str) > $i && $input_str[$i] != ";");

			// Go to the next character and go straight to the end of the
			// loop so the next character is NOT analyzed below
			$i++;
			continue;				
		}
		
		// We are not in a HTML tag or encoded char, check if the current
		// char is a French punctuation character
		if ($input_str[$i] == "?" ||
			$input_str[$i] == "!" ||
			$input_str[$i] == ":" ||
			$input_str[$i] == ";"
		   ) {
			// Char is a French punctuation character, check if there's 
			// already an non-break space (&nbsp, length = 6) in front of it.
			
			// If we are already at a location in the string where an &nbsp;
			// can be in front of the punctuation char 
			if ($i > 6) {
				if (substr_compare ($input_str, '&nbsp;', $i-6, 6) != 0) {
					// No &nbsp in front, add one
					$log->lwrite('AUTO-FIX 3: !!! INSERTING !!! &nbsp; at loc ' . $i);
					$input_str = substr_replace($input_str, '&nbsp;', $i, 0);
					$i += 6;
				}
				else
				{
					//$log->lwrite('AUTO-FIX 3: &nbsp; already present at loc ' . $i);					
				}
			}
			else // String too short, no &nbsp in front, add it
			{
				$log->lwrite('AUTO-FIX 3: String short, !!! INSERTING !!!  &nbsp;');
				$input_str = substr_replace($input_str, '&nbsp;', $i, 0);
				$i += 6;
			}
		}
		
		$i++;		
	} while (strlen($input_str) > $i);

	return($input_str);
}

/**
 * fixHttpAndHttpsReferers
 *
 * This function should be called after fixFrenchPunctuation2(). It searches
 * and removes &nbsp; spaces inserted by the previous function before the ':'
 * char in http:// and https:// and before the '?' character which is used
 * in URLs with parameters.
 *
 * @param $input_string
 *
 * @return $output_string with changed quotations
 */

function fixHttpAndHttpsReferers($input_str){

	$log = new Logging();

	$count = 0;
	$spaceAndChar = array ("http&nbsp;:"  => "http:",
                 		   "https&nbsp;:" => "https:",
			               "ark&nbsp;:"   => "ark:",
	                       "/&nbsp;?"     => "/?"
	);

	foreach ($spaceAndChar as $search_str=>$replace_str):
	$input_str = str_replace ($search_str, $replace_str, $input_str, $count);
	if ($count > 0) {
		$log->lwrite('HTTP-FIX: !!! CHANGED !!!  "' . $search_str . '" ' .
				$count . ' times');
	}
	endforeach;

	//$log->lwrite("AUTO-FIX 1 - Outpt: " . $input_str);

	return($input_str);
}


/**
 * fixApostrophe
 *
 * Searches the string for the ' character and replaces it with the HTML
 * equivalent of "&#39;". This is necessary as apostrophes in normal input 
 * field return strings are not converted by default 
 *
 * @param $input_string
 *
 * @return $output_string with changed quotations
 */
function fixApostrophe ($input_str) {

	$log = new Logging();
	$count = 0;

	$input_str = str_replace ("'", "&#39;", $input_str, $count);
	
	if ($count > 0) {
		$log->lwrite('AUTO-FIX 4: Apostrophe converted to &#39; ' . $count . 
				     ' times');
		//$log->lwrite('New text: ' . $input_str);
	}
		
	return($input_str);
}