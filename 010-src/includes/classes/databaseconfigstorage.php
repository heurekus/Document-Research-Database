<?php
/**
* databaseconfigstorage.php
*
* This class stores and retrieves the configuration and fields of the
* main DRDB. database table and contains functions to update the
* configuration information in the database.
*
* @version    1.0 2021-05-10
* @package    DRDB
* @copyright  Copyright (c) 2018 Martin Sauter
* @license    GNU General Public License
* @since      Since Release 1.0
*/

// This module global variable temporarily stores config parameters
// to reduce the number of times the SQL database needs to be queried
// for a web page request. 
$tmp_storage_for_config_params = array();

class DatabaseConfigStorage
{
	 
/**
 * createInitialTable()
 *
 * Creates the initial 'database config storage' table.
 *
 * The table will only be created and the record only added if the table
 * does not already exit, so nothing is overwritten in case this method 
 * is called accidentally.
 *
 * @param none
 *
 * @return boolean, true = succes, false = fail, database table was not
 * created (e.g. due to a database error or because it already exists)
 *
 */
public static function createInitialTable() {

	$log = new Logging();

	// Create SQL query. 
	// Note: config_type will be the index so it
	// has to be length limited. I.e. varchar(100)
	// is used instead of text (BLOB).
	$query = 'CREATE TABLE `drdb_config` (' . PHP_EOL;	
	$query .= '`config_type` varchar(100) NOT NULL,' . PHP_EOL;
	$query .= '`config_value` text,' . PHP_EOL;
	$query .= 'PRIMARY KEY (`config_type`)' . PHP_EOL;
	$query .= ') DEFAULT CHARSET=utf8;' . PHP_EOL;	
	
	$log->lwrite('SQL, create table: ' . $query);

	try {
		// Get the connection and run the query
		$connection = DRDB::getConnection();
		$result_obj = $connection->query($query);

		if (!$result_obj) {
			throw new Exception($connection->error);
		}

		$log->lwrite('Creation of the db_config table successful');


	}

	catch(Exception $e) {

		if (!$result_obj) {
			$log->lwrite('SQL ERROR: ' . $connection->errno . ' : ' . $connection->error);

		} else {
			$log->lwrite('ERROR: Exception was raised');
		}

		return false;
	}

	// Initialize the new table with values from database-structure.php
	return self::populateInitialDbFieldsConfig();
}

/**
 * populateInitialDbFieldsConfig()
 *
 * Get the initial main database columns and config info from the
 * database-structure.php config file and write it into the drdb_config
 * table.
 *
 * @param none
 *
 * @return boolean, true = success, otherwise false
 *
 */
private static function populateInitialDbFieldsConfig() {
    
    $log = new Logging();
    
    global $doc_db_description;
    
    if (count($doc_db_description) < 1) {
        $log->lwrite('ERROR: database fields in database-structure.php');
        return false;
    }

    return self::updateMainDbFieldConfig($doc_db_description);

}


/**
 * updateMainDbFieldConfig()
 *
 * Take the given array that contains the configuration information about
 * columns of the main table and write it into the drdb_config table.
 *
 * @param array, description of the main database table columns
 *
 * @return boolean, true = success, false = failure
 *
 */
public static function updateMainDbFieldConfig($doc_db_description_array){
    
    $log = new Logging();
    
    if (!isset($_SERVER['PHP_AUTH_USER'])) {
        $log->lwrite('Error - No authenticated user...');
        return(false);
    }
    
    // Create a JSON encoded string from the database description array
    // in database-structure.php
    $doc_db_descriptionJSON = json_encode($doc_db_description_array);
    $log->lwrite("Updating database fields");    
    
    return self::setCfgParam("main_db_fields", $doc_db_descriptionJSON);

}

/**
 * setCfgParam()
 *
 * Write a config parameter to the database.
 * *
 * @param string, parameter name
 * @param string, parameter value 
 *
 * @return boolean, true = success, false = failure
 *
 */
public static function setCfgParam($param_name, $param_value) {
    
    $log = new Logging();
    
    if (!isset($_SERVER['PHP_AUTH_USER'])) {
        $log->lwrite('Error - No authenticated user...');
        return(false);
    }
    
    // Get the Database connection
    $connection = drdb::getConnection();
    
    // Update existing configuration record for the user in the database
    $query = "REPLACE INTO drdb_config(config_type, config_value) " .
             "VALUES (?, ?)";
    
    $prep_query = $connection->prepare ($query);
    $prep_query->bind_param("ss", $param_name, $param_value);
    
    $log->lwrite('SQL: ' . $query);
    $log->lwrite('config_type: ' . $param_name);
    $log->lwrite('config_value: ' . $param_value);
    
    // Run the MySQL statement
    if ($prep_query->execute()) {
        $log->lwrite('Parameter successfully saved in drdb_config table');

        // Delete the paramater/value from the temporary cache array
        // if it exists
        global $tmp_storage_for_config_params;
        unset($tmp_storage_for_config_params[$param_name]);
        
        // One row is changed if a new entry is created. Two rows are changed when
        // an entry is updated as REPLACE INTO deletes the old record and creates it
        // again.
        if ($prep_query->affected_rows < 1 or $prep_query->affected_rows > 2) {
            $log->lwrite('Rows changed: ' . $prep_query->affected_rows);
            $log->lwrite('Note: 0 could mean unaltered or error');
        }
        $prep_query->close();
    }
    else {
        $log->lwrite('ERROR: Config parameter not updated. Unable to save record');
        $log->lwrite('SQL ERROR: ' . $prep_query->error);
        $prep_query->close();
        return false;
    }
    
    return true;
}


/**
 * getDbFieldConfig()
 *
 * Reads the main database table configuration from the database
 * and returns an array with all information.
 *
 * @param none.
 *
 * @return array with the database table configuration.
 *
 */

public static function getDbFieldConfig() {
    
    $log = new Logging();

    $local_doc_db_descriptionJSON = self::getCfgParam('main_db_fields');

    //$log->lwrite('Config: ' . $local_doc_db_descriptionJSON);
    
    if (!$local_doc_db_descriptionJSON)
    {
        $log->lwrite('ERROR: main table config does not exist in database');
        $log->lwrite('ERROR: This should only happen when the complete DB is NOT initialized');
        $log->lwrite('ERROR: Returning main DB table info from config file instead.');
        
        GLOBAL $doc_db_description;        
        return $doc_db_description;
    }
    
    // Convert from JSON string to the PHP array representation.
    $local_doc_db_description = json_decode($local_doc_db_descriptionJSON, true);
   
    return $local_doc_db_description;
}

/**
 * getCfgParam()
 *
 * Get a DRDB configuration parameter that can be edited
 * by an admin user. The method will look for the parameter
 * name given as first parameter as follows:
 *
 *  * Checks the TEMPORARY variable storage (module global array)
 *    if the parameter is present. If so it is returned.
 *    
 *  * If not, it checks the drdb_config table. If parameter
 *    is present it is writen to the database and returned.
 *    
 *  * The legacy $CONFIG global variable from config.php
 *    is checked. If the parameter is present it is returned.
 *    If not present, false is returned.
 *
 * @param Parameter name (=table 'drdb_config', 'config-type) 
 *
 * @return string or false if the parameter was not found
 *
 */

public static function getCfgParam($param_name) {
    
    global $tmp_storage_for_config_params;
    
    $log = new Logging();
    
    // $log->lwrite('Getting parameter (config_type): ' . $param_name);
    
    // Check if the requested parameter was already retrieved
    // from the database in a previous invocation. If so, return
    // the temporary copy that was previously stored.
    if (isset($tmp_storage_for_config_params[$param_name])) {
        // $log->lwrite('Returning stored parameter');
        return $tmp_storage_for_config_params[$param_name];
    }        
    
    // Get the database connection and get the user's configuration
    $connection = drdb::getConnection();

    $query = 'SELECT * FROM drdb_config WHERE config_type=?';

    $prep_query = $connection->prepare ($query);
    if ($prep_query === false) {
        
        // Looks like the database table does not yet exist.
        // Report the error in the log and skip the database
        // query.
        $log->lwrite("ERROR: Preparing the query has failed... ");
        $log->lwrite("ERROR: This should only happen before the database upgrade");
        $log->lwrite("ERROR: or before initial creation of the database.");
        
    } else {       
        
        // The database table exists, let's run the query
        $result = $prep_query->bind_param("s", $param_name);
        // $log->lwrite("Result of bind_param(): " . $result);
        
        //$log->lwrite('SQL: ' . $query);
        
        $result_obj = $prep_query->execute();
        
        if (!$result_obj) {
            $log->lwrite('Database access error, this should not have happened!');
            $prep_query->close();
            return false;
        }
        
        // Get the parameter from the SQL result
        $result = $prep_query->get_result();
        $sql_row = $result->fetch_assoc();
        $prep_query->close();
        
        if ($sql_row !== NULL) {
            $log->lwrite('Return value from database');
            $tmp_storage_for_config_params[$param_name] = $sql_row['config_value'];
            return $sql_row['config_value'];
        }
        
    } // End of database query section
    
    // The parameter was not in the database, let's see if it's in
    // the legacy global array.    
    global $CONFIG;
    
    if (isset($CONFIG[$param_name])) {
        // $log->lwrite('Return param from global $CONFIG');
        $tmp_storage_for_config_params[$param_name] = $CONFIG[$param_name];
        return $CONFIG[$param_name];
    }
        
    // No luck anywhere, parameter was not found.
    $log->lwrite('ERROR: Config parameter not found: ' . $param_name);
    return false;
}

/**
 * clearConfigCache()
 *
 * This function should be called when the config changes during the
 * request and the new config should be loaded from the database
 * for the rest of the request.
 *
 * @param none
 *
 * @return null
 *
 */

public static function clearConfigCache() {
    
    global $tmp_storage_for_config_params;
    
    $tmp_storage_for_config_params = array();
}

} // end of class















