<?php
/**
 * ConventionsInfo.php
 *
 * This file contains a class that deals with the conventions record in
 * the main database table and has methods to present conventions information, 
 * e.g. in the 'details' and 'edit all' pages.
 * 
 * Important: Pages using insertInfoButton() MUST initialize 
 *            Bootstrap 3 tooltips via js/init_tooltips.js as they
 *            are otherwise not shown correctly.
 *
 * @version    1.0 2019-05-09
 * @package    DRDB
 * @copyright  Copyright (c) 2014-19 Martin Sauter
 * @license    GNU General Public License
 * @since      Since Release 1.0
 */

require_once 'includes/field-empty-and-trim-functions.php';

define ('CONVENTIONS_RECORD_ID', -10);


class ConventionsInfo
{

protected $log = NULL;
protected $conventions_record = NULL;

/**
 * __construct()
 *
 * Initializes the object by loading the conventions record into a variable.
 * If the conventions record does not exist, the constructor creates an
 * empty entry in the database.
 *
 * @param none
 *
 * @return null
 *
 */
function __construct() {
    
    $this->log = new Logging();
    $this->log->lwrite ('Initializing Conventions info object');
    
    // Get the 'conventions' for all record fields from the database
    $this->conventions_record = DocRecord::getRecord(CONVENTIONS_RECORD_ID);
    
    if (!$this->conventions_record) {
    
        $this->log->lwrite('No Conventions Info record exists, creating a new one');
        
        // Note: No check is done whether creation was successful or not.
        // If not successful, all requests for info text will be answered
        // with '----'
        $this->createEmptyConventionsRecord();
        
    }
}
    
/**
 * insertInfoButton()
 *
 * Generates and returns HTML code that can be inserted in a web page 
 * to show a clickable conventions 'info' glyphicon icon. When the 
 * mouse hovers over it, the conventions info is shown. Clicking on the 
 * info button opens the edit conventions record page.
 *
 * @param string, the name of the field of a DRDB record in the main database
 *        table. It is used as an index to find the info text.
 *
 * @return string, HTML code of the info button that can be inserted
 *         in a web page.
 *
 */
 
public function insertInfoButton($field_name) {
   
    $conventions_txt = $this->getConventionsText($field_name);
    
    $html_result = '<a href="index.php?' .
        'content=bs_record_details&id=' . CONVENTIONS_RECORD_ID . '#dir_' .
        $field_name .
        '" target="_blank"><span class="glyphicon glyphicon-info-sign" ' .
        'data-toggle="tooltip" data-placement="left" data-html="true" title="' .
        $conventions_txt . '"></span></a>';
    
    return $html_result;
}


/**
 * getConventionsText()
 *
 * Returns the HTML encoded convention info text for the given main database
 * table record field name.
 *
 * @param string, the field name
 *
 * @return string, HTML code for the conventions info button
 *
 */

protected function getConventionsText ($field_name) {
    
    if (!$this->conventions_record) {
            return "-----";
    }

    $conventions_txt = $this->conventions_record->getField($field_name);

    if (isFieldEmpty($conventions_txt)) {
            return "-----";
    }
    
    return $conventions_txt;
}


/**
 * createEmptyConventionsRecord()
 *
 * @param none
 *
 * @return null
 *
 */

protected function createEmptyConventionsRecord() {
    
    $item = array();
    $doc = new DocRecord($item);
    
    $this->log->lwrite('Trying to create a new CONVENTIONS record!');
    
    // Try to create a new conventions record. As it is of no consequence
    // whether this succeeds or not, no check is done afterward.
    $doc->addRecord(CONVENTIONS_RECORD_ID);
    
}

} // end of class
