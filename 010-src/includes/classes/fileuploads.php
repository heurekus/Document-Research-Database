<?php
/**
* fileuploads.php
*
* This class offers functions to deal with uploading and managing
* files that live alongisde the standard database records. The
* files are not stored in the database but in subdirectories of
* the /uploads folder. The subdirectory name for files of a 
* database record is the unique hash value generated for each
* database entry. nside the subdirectory for a database record
* an layer of subdirectories are used to store files for
* individual database fields.
*
* @version    1.0 2022-06-04
* @package    DRDB
* @copyright  Copyright (c) 2022 Martin Sauter
* @license    GNU General Public License
* @since      Since Release 1.0
*/

require_once 'includes/file-upload.php';

define('UPLOAD_BASE_DIR', "uploads");

// Set the max file size to 500 MB. Can be changed.
define('MAX_DRDB_UPLOAD_FILE_SIZE', 500000000);

class FileUploads
{
	 
/**
 * HandleUploadedFiles()
 *
 * Handles recently uploaded files via an HTTP POST request. All information
 * on the uploaded files are contained in the HTTP POST PHP variables.
 *
 *  - It will store the the referenced files subdirectories of /uploads. 
 * 
 *  - The first subdirectory level is the hash_id of the database record
 * 
 *  - If a field_name is given for a file upload, a directory with the field_name
 *    is created and files are stored there.
 * 
 *  - If no field_name is given, the files are stored in the base hash_id directory.
 * 
 *
 * @param none
 *
 * @return List of strings that indicate the result of the action. The first
 *         string is 0 for failure and 1 for success. The second string can contain
 */
public static function HandleUploadedFiles() {

    $log = new Logging();

	// Check the token that was set by the requesting page to prevent
	// Cross Site Request Forgery (CSRF)
	if (checkAntiCsrfProtection() == false) {
	
        $log->lwrite('ERROR: Invalid token, page processig aborted!');
        return(array("0", "Sorry, go back and try again. " . 
                     "There was a security issue"));			
	}     

    $log->lwrite('Number of files: ' . count($_FILES['filename']['name']));

    $hash_id = self::GetHashIDofDatabaseRecord($log);

    if ($hash_id === false) {
        return(array("0", "File upload error: Length of hash string too short"));
    }

    $log->lwrite("Hash ID of record: " . $hash_id);

    // Get the field name to which the attachments should be connected to
    // The field name is part of the form, hence it is a HTTP POST
    // variable.

    $field_name = self::GetFieldNameForAttachment($log);
    if ($field_name === false) {
        return(array("0", "File upload error: Field Name does not exist: " . $field_name));
    }

    // Assemble the upload directory path
    $upload_dir = UPLOAD_BASE_DIR . DIRECTORY_SEPARATOR . $hash_id;
    
    // If the file is for a particular database field, extend the path
    if ($field_name != "") $upload_dir = $upload_dir . DIRECTORY_SEPARATOR . $field_name;

    $log->lwrite("Upload DIR: " . $upload_dir);
    
    // If the target directory for the files does not yet exist, create it
    $create_ok = true;
    if (!is_dir($upload_dir)) {
        $create_ok = mkdir($upload_dir, 0744, true);
    }

    if ($create_ok != true) {
        return(array("0", "File upload error: Unable to create directory"));
    }

    // Loop over all files
    for ($i = 0; $i < count($_FILES['filename']['name']); $i++) {

        $filename = $_FILES['filename']['name'][$i];

        $filename = self::SanitizeFilename($filename, $log);

        if ($_FILES['filename']['error'][$i] == 4) {
            $log->lwrite("Error during file upload: " . $filename);
        }

        // Note: Existing files will be overwritten without warning if the same 
        // filename is used.        
        $copy_result = drdb_handle_file_upload ('filename', $upload_dir, 
                                                 $filename,
                                                 MAX_DRDB_UPLOAD_FILE_SIZE, $i);
        if ($copy_result != true) {
            return(array("0", "File upload error: Unable to move file"));
        }
    }

    return(array("1","File Upload OK"));
}


/**
 * GetFileList()
 *
 * Get a list of files attached to a field of a database record
 *
 * @param string, the hash_id of the database record
 * 
 * @param string, optional, the database field name. If omitted, 
 * the file list of the root directory (hash_id only) will be returned.
 *
 * @return array, with a list of strings of paths and filenames or an 
 * empty array if the directory is empty or does not exist.
 * 
 */
public static function GetFileList($hash_id, $field_name = "") {

    $log = new Logging();

    $file_list = [];

    if (strlen($hash_id) < 10) {
        $log->lwrite("Error: Invalid Hash ID: " . $hash_id);
        return $file_list;
    }

    // Prevent directory traversal attacks
    if (strpos($hash_id, "..") !== false) {
        $log->lwrite("Error: hash_id, contains '..' " . 
                     " - directory traversal attack?: " . $hash_id);
        return $file_list;        
    }

    //$log->lwrite("Hash ID of record: " . $hash_id);

    $upload_dir = UPLOAD_BASE_DIR . DIRECTORY_SEPARATOR . $hash_id;

    if ($field_name != "") {

        // Prevent directory traversal attacks
        if (strpos($field_name, "..") !== false) {
            $log->lwrite("Error: field_name, contains '..' " . 
                         " - directory traversal attack?: " . $hash_id);
            return $file_list;        
        }

        $upload_dir = $upload_dir . DIRECTORY_SEPARATOR . $field_name;
    }

    //$log->lwrite("Upload DIR: " . $upload_dir);
    
    if (!is_dir($upload_dir)) {
        //$log->lwrite("Directory doesn't exist, no files in the directory");
        return $file_list;
    }

    $dir_entries = array_diff(scandir($upload_dir), array('..', '.'));

    if (count($dir_entries) < 1) {
        //$log->lwrite("Directory exists, but no files in the directory");
        return $file_list;
    }

    // Go through the list of files and directories and create an array
    // that only contains entries for files.
    foreach ($dir_entries as $dir_entry) {

        if (is_dir($upload_dir . DIRECTORY_SEPARATOR . $dir_entry)) continue;

        $output_dir_entry["path"] = $upload_dir . DIRECTORY_SEPARATOR;
        $output_dir_entry["filename"]=$dir_entry;

        array_push($file_list, $output_dir_entry);
    }

    return $file_list;
}


/**
 * HandleFileDeletion()
 *
 * Handles a file attachment deletion request. All parameters required
 * are in the URL and the POST parameters of the request.
 *
 * @param none
 *
 * @return List of strings that indicate the result of the action. The first
 *         string is 0 for failure and 1 for success. The second string can contain
 */

public static function HandleFileDeletion() {

    $log = new Logging();

    $log->lwrite("Handle file deletion request");

	// Check the token that was set by the requesting page to prevent
	// Cross Site Request Forgery (CSRF)
	if (checkAntiCsrfProtection() == false) {
	
        $log->lwrite('ERROR: Invalid token, page processig aborted!');
        return(array("0", "Sorry, go back and try again. " . 
                     "There was a security issue"));			
	} 

    // Now get the hash id of the database record
    $hash_id = self::GetHashIDofDatabaseRecord($log);

    if ($hash_id === false) {
        return(array("0", "File delete error: Length of hash string too short"));
    }

    $log->lwrite("Hash ID of record: " . $hash_id);    

    // Get the field name to which the file is attached from the HTTP POST part
    $field_name = self::GetFieldNameForAttachment($log);
    if ($field_name === false) {
        return(array("0", "File delete error: Field Name does not exist: " . $field_name));
    }

    // Assemble the upload directory path
    $upload_dir = UPLOAD_BASE_DIR . DIRECTORY_SEPARATOR . $hash_id;
    
    // If the file to delete is connected to a particular database field, extend the path
    if ($field_name != "") $upload_dir = $upload_dir . DIRECTORY_SEPARATOR . $field_name;

    $log->lwrite("Upload DIR: " . $upload_dir);

    // Get file name from HTTP Post variable
    $filename = "";
    if (isset($_POST['file_name'])) {
        $filename = trim(filter_input(INPUT_POST, 'file_name', FILTER_SANITIZE_STRING));
    }

    if ($filename == "") {
        return(array("0","File delete error: No filename was given"));
    }

    // Sanitize filename to prevent directory traversal attacks
    $filename = self::SanitizeFilename($filename, $log);

    $path_and_filename = $upload_dir . DIRECTORY_SEPARATOR . $filename;

    // And finally, delete the file
    if (!unlink($path_and_filename)) {
        $log->lwrite("ERROR: Unable to delete file: " . $path_and_filename);
        return(array("0","File delete error: Unable to delete file"));
    }

    return(array("1","File deletion OK"));
}


/**
 * GetHashIDofDatabaseRecord()
 *
 * Returns the hash id of the database record whose ID is in the
 * http request.
 *
 * @param log object
 *
 * @return string with the hash id or false if there was a problem.
 */
private static function GetHashIDofDatabaseRecord($log) {

    // Get the hash value of the database entry
    if (!isset($_GET['id'])) {
        $log->lwrite('ID not present, aborting');
        return(array("0", "File upload error: ID not present for file upload"));
    }
    
    // Get the ID from the URL --> HTTP GET
    $id = (int) $_GET['id'];

    $log->lwrite("ID of record: " . $id);

    $item = DocRecord::getRecord($id);
    $hash_id = $item->getField('hash_id');

    if (strlen($hash_id) < 10) {
        return false;
    }

    return $hash_id;
}

/**
 * DoesFieldNameExist()
 *
 * Internal service function:
 * 
 * Compare the given field name with all field names of the main table
 * of the database. Note: This is NOT the field name as shown to the
 * user but the internal field name (typically F0...)
 *
 * @param string, database field name
 *
 * @return boolean, true if the field exists, otherwise false
 */
private static function DoesFieldNameExist($received_field_name) {

    $local_doc_db_description = DatabaseConfigStorage::getDbFieldConfig();
 
    foreach ($local_doc_db_description as $field_print_name=>$field_options):

        $field_name = $field_options[DB_FIELD_NAME];
        if (strcmp($field_name, $received_field_name) == 0) return true;

    endforeach;

    return false;
}


/**
 * GetFieldNameForAttachment()
 *
 * Internal service function:
 * 
 * Gets the field name to which a file should be attached to. In case
 * the POST parameter is NOT present, an empty string will be returned
 * so the file is attached to the general database record. 
 *
 * @param log object
 *
 * @return The field name, an empty string or false if there was a problem.
 * 
 */
private static function GetFieldNameForAttachment($log) {

    $field_name = "";
    if (isset($_POST['file_upload_field_name'])) {

        $field_name = trim(filter_input(INPUT_POST, 'file_upload_field_name', FILTER_SANITIZE_STRING));
        $log->lwrite("Field Name: " . $field_name);

        // Make sure the field name exists to prevent directory traversal attacks.
        if (!self::DoesFieldNameExist($field_name)) {
            return false;
        }
    }

    return $field_name;
}

/**
 * SanitizeFilename()
 *
 * Internal service function:
 * 
 * To prevent directory traversal attacks, replace some characters with an underscore.
 *
 * @param string, filename
 * @param log object
 *
 * @return The sanitized filename
 * 
 */
private static function SanitizeFilename($filename, $log) {

    $log->lwrite("Filename (original): " . $filename);

    // Replace special characters in the filename that are problematic
    // in HTTP requests.
    $filename = str_replace(array(" ","&","'","+","#","%"), "_", $filename);
    // The '/' char is also not allowed, directory traversal attack
    $filename = str_replace("/", "_", $filename);
    // And we don't want "..", directory traversal attack possible, so replace
    // that as well. Note: This is valid in a filename, perhaps except at the
    // beginning, but let's be on the safe side here.
    $filename = str_replace("..", "_.", $filename);
    $log->lwrite("Filename (clean)   : " . $filename);

    return $filename;
    
}

} // end of class
?>
