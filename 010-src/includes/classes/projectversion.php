<?php
/**
* projectversion.php
*
* This class stores and retrieves the project version from a table
* in the database. The version number of the project is required, for example,
* to modify database tables after a server software update that requires
* an updated database structure.
*
* @version    1.0 2018-07-18
* @package    DRDB
* @copyright  Copyright (c) 2018 Martin Sauter
* @license    GNU General Public License
* @since      Since Release 1.0
*/

class ProjectVersion
{
	 
/**
 * createInitialTable()
 *
 * Creates the initial 'version' database table and creates the version
 * number table entry with an initial value.
 *
 * The table will only be created and the record only added if the table
 * does not already exit, so nothing is overwritten in case this method 
 * is called accidentally.
 *
 * @param none
 *
 * @return boolean, true = succes, false = fail, database table was not
 * created (e.g. due to a database error or because it already exists)
 *
 */
public static function createInitialTable() {

	$log = new Logging();

	// Two querries, one to create the table and one to populate an
	// initial value
	$query = 'CREATE TABLE `version` (`version` INT NOT NULL)';
	$initial_version = "INSERT INTO `version` (`version`) VALUES ('"  . 
	                    DRDB_DB_VERSION . "')";

	$log->lwrite('SQL, create table: ' . $query);
	$log->lwrite('SQL, initial DB version:' . $initial_version);

	try {
		// Get the connection and run the query
		$connection = DRDB::getConnection();
		$result_obj = $connection->query($query);

		if (!$result_obj) {
			throw new Exception($connection->error);
		}

		$log->lwrite('Creation of the version table successful');

		$result_obj = $connection->query($initial_version);
		
		if (!$result_obj) {
			throw new Exception($connection->error);
		}
		
		$log->lwrite('Inserting initial version number successful');

	}

	catch(Exception $e) {

		if (!$result_obj) {
			$log->lwrite('SQL ERROR: ' . $connection->errno . ' : ' . $connection->error);

		} else {
			$log->lwrite('ERROR: Exception was raised');
		}

		return false;
	}

	return true;
}


/**
 * updateVersionNumber()
 *
 * @param int, new version number
 *
 * @return boolean, true = success, false = update failure
 * 
 */

public static function updateVersionNumber($new_version) {

	// Get the Database connection
	$connection = drdb::getConnection();

	$query = "UPDATE `version` SET version='". (int) $new_version . "'";
	
	$log = new Logging();
	$log->lwrite('SQL: ' . $query);

	// Run the MySQL statement
	if ($connection->query($query)) {

		$log->lwrite('Version number in database updated to v.' . $new_version);
		return true;

	} else {
		$log->lwrite('Error updating version number in database');
		false;
	}
}
	
/**
 * getVersionNumber()
 *
 * Returns the current version number from the database
 *
 * @return mixed, version number (int) or false (boolean) if there 
 * was a problem getting the version number.
 *
 */

public static function getVersionNumber() {

	$log = new Logging();
		
	// Get the database connection and get the user's configuration
	$connection = drdb::getConnection();
	$query = 'SELECT version FROM version';
	$log->lwrite('SQL: ' . $query);
	$result_obj = $connection->query($query);
	
	if (!$result_obj) {
		$log->lwrite('Database access error');
		return(false);
	}

	$version_sql_result = $result_obj->fetch_assoc();
	
	return $version_sql_result['version'];
		
}	
	
} // end of class
