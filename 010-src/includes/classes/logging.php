<?php
/**
 * logging.php
 *
 * The class to write text to the log file
 *
 * @version    1.1 2017-01-21
 * @package    DRDB
 * @copyright  Copyright (c) 2014-2018 Martin Sauter
 * @license    GNU General Public License
 * @since      Since Release 1.0
 */

class Logging {
    // declare log file and file pointer as private properties
    private $log_file, $fp;    
    private $log_file_default = 'log/drdb-logfile.txt';
    
    // set log file (path and name)
    public function lfile($path) {
        $this->log_file = $path;
    }
    
    // write message to the log file
    public function lwrite($message) {
        // if file pointer doesn't exist, then open log file
        if (!is_resource($this->fp)) {
            $this->lopen();
        }
        
        // define current time and suppress E_WARNING if using the system 
        // TZ settings (don't forget to set the INI setting date.timezone)
        $time = @date('[Y/m/d H:i:s]');        
        $client_ip = $_SERVER['REMOTE_ADDR'];        
        $request_uri = $_SERVER["REQUEST_URI"];        
        $user = $_SERVER['PHP_AUTH_USER'];
        
        // Get the name of the calling function for the debug output
        // Note: if the code is not from "within" a function (e.g. just
        // a php file that executes code, "incluce()" is shown as
        // calling function.
        $dbt=debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS,2);
        $caller = isset($dbt[1]['function']) ? 
                        $dbt[1]['function'] : null;
               
        fwrite($this->fp, "$time $client_ip $user $caller() $message" . PHP_EOL);
        
        fclose ($this->fp);
    }

    public function checkLogFileSizeAndArchive($maxFileSizeInMB) {
    	// define log file from lfile method or use previously set default
    	$lfile = $this->log_file ? $this->log_file : $this->log_file_default;
    	$archive_filename = 'log/' . $t = date('Y-m-d-H-i-s') . '-drdb-log.tar';

    	// Set a minimum file size if non or unreasonably low number is given.
    	if ($maxFileSizeInMB < 1) {
    		$maxFileSizeInMB = 10;
    	}
    	    	
    	$filesize = filesize($lfile);
    	
    	// Exit if the file size limit has not yet been exceeded
    	if ($filesize < ($maxFileSizeInMB*1024*1024)) return;

    	$this->lwrite('Log file size: ' . $filesize . ', ' . 
    			      'archiving it to ' . $archive_filename);
    	$this->lwrite('!!!! STOP THE LOG VIEWER AND RESTART !!!!');
    	
    	// Create a new tar file and save the current log file into it
    	$a = new PharData($archive_filename);
    	$a->addFile($lfile);
    	
    	// Compress the tar archive into a new file, .gz will be added 
    	// to the filename
    	$a->compress(Phar::GZ);
    	
    	// delete the uncompressed file log file
    	unlink($archive_filename);
    	
    	// close and delete the current log file
    	$this->lclose();
    	unlink($lfile);
    	
    	$this->lwrite('Log archived, starting new log!');
   	
    	return;
    }
    
    // Open log file (private method)
    private function lopen() {
  	
        // define log file from lfile method or use previously set default
        $lfile = $this->log_file ? $this->log_file : $this->log_file_default;
        // open log file for writing only and place file pointer at the end 
        // of the file. If the file does not exist, try to create it.
        $this->fp = fopen($lfile, 'a') or exit("Can't open $lfile, " . 
        "please check directory permissions!");
    }
    
    // Close the log file (private method)
    private function lclose() {
   	
    	if (is_resource($this->fp)) {
    		fclose($this->fp);
    	}
    }
        
}