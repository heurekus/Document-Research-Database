<?php
/**
 * file-upload.php
 *
 * Provides a generic funtion to handle uploaded files and put them into
 * a target directory.
 *
 * @version    1.0 2019-03-18
 * @package    DRDB
 * @copyright  Copyright (c) 2014-19 Martin Sauter
 * @license    GNU General Public License
 * @since      Since Release 1.0
 */

/**
 * drdb_handle_file_upload()
 *
 * The function moves an uploaded file from the temporary PHP directory to
 * a destination directory if the following conditions are met:
 * 
 *  - The extension of the uploaded filename and the target filename are
 *    identical (e.g. both are '.odt')
 *    
 *  - File size is below the given max file size.
 *
 * @param string, PHP id of the uploaded file given in the original HTML
 *        form in the name part. Example in which 'fileToUpload' is the ID:
 *        '<input type="file" name="fileToUpload" id="fileToUpload">
 *        
 * @param string, target directory without a directory separater (e.g. '/')
 *        at the end. The target directory has to be given from the base of the
 *        web project without a leading directory separator!
 *        
 * @param string, the target file name. Note: The filename extension is
 *        compared against the filename extension of the uploaded file.
 *        
 * @param number, the maximum file size in bytes
 * 
 * @param number, (optional). if not given, $_FILES is a simple array. If
 *        given and > -1 $_FILES describes several uploaded files. The number
 *        then indicates which file to treat.   
 * 
 *
 * @return mixed, true if successful or a string with an error message
 *
 */
function drdb_handle_file_upload ($php_id_uploaded_file, $target_dir, 
                                  $target_file_name, $max_file_size,
                                  $file_num = -1) {

    $log = new Logging();
    
    // Definition of the target directory and filename
    $target_file_name = $target_dir . DIRECTORY_SEPARATOR . $target_file_name;
    $target_file_type = strtolower(pathinfo($target_file_name, PATHINFO_EXTENSION));
    
    $log->lwrite('Target dir: ' . $target_dir);
    $log->lwrite('Target path and filename: ' . $target_file_name);
    $log->lwrite('Target file type: ' . $target_file_type);
    
    // Get information about the uploaded file
    if ($file_num < 0) {
        $uploaded_file_name = basename($_FILES[$php_id_uploaded_file]["name"]);
    } else {
        $uploaded_file_name = basename($_FILES[$php_id_uploaded_file]["name"][$file_num]);
    }
    
    $uploaded_file_type = strtolower(pathinfo($uploaded_file_name, PATHINFO_EXTENSION));
    
    $log->lwrite('Name of uploaded file: ' . $uploaded_file_name);
    $log->lwrite('Type of uploaded file: ' . $uploaded_file_type);
        
    // Check if this was a HTTP Post with a submit button
    if(!isset($_POST["submit"])) {
        
        $err_msg = 'ERROR: File Upload error: Submit button was not present';
        $log->lwrite($err_msg);
        return $err_msg;
    }
    
    // Check file size and reject if it is too big
    if ($file_num < 0) {
        $file_size = $_FILES[$php_id_uploaded_file]["size"];
    } else {
        $file_size = $_FILES[$php_id_uploaded_file]["size"][$file_num];
    }
    
    if ($file_size > $max_file_size) {
        
        $err_msg = "Sorry, file is too large, max size is " . $max_file_size . " bytes";
        $log->lwrite($err_msg);
        return $err_msg;
    }
    
    // Only files with suffixes that match the target's filename suffix are allowed
    if($uploaded_file_type != $target_file_type) {
        
        $err_msg = "File type not allowed";
        $log->lwrite($err_msg);
        return $err_msg;
    }
    
    // Everything has checked out, lets try to copy the file from the temp
    // directory to the intended destination directory.

    if ($file_num < 0) {
        $tmp_name = $_FILES[$php_id_uploaded_file]["tmp_name"];
    } else {
        $tmp_name = $_FILES[$php_id_uploaded_file]["tmp_name"][$file_num];
    }

    if (move_uploaded_file($tmp_name, $target_file_name)) {
        $log->lwrite('File upload successful');
    }
    
    return true;
    
}



?>