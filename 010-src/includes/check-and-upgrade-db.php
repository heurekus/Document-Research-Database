<?php

/**
 * check-and-upgrade-db.php
 *
 * Contains the functionality to upgrade the database after a software upgrade.
 * 
 * checkDatabaseVersionAndUpgrade() is executed at every request. It compares
 * which project version number is currently stored in the database. If
 * it is not the same as DRDB_DB_VERSION defined in init.php it will
 * upgrade the database step by step. This way it is possible to upgrade
 * over several database changes, i.e. over several software versions. 
 * In other words, all upgrade steps are done one after another and the
 * administorator does not have to upgrade the software to intermediate
 * states. 
 *
 * @version    1.0 2018-07-19
 * @package    DRDB
 * @copyright  Copyright (c) 2018 Martin Sauter
 * @license    GNU General Public License
 * @since      Since Release 1.0
 */

function checkDatabaseVersionAndUpgrade() {

	$log = new Logging();
	$version = ProjectVersion::getVersionNumber();
	
	if ($version === false) {
		$log->lwrite('ERROR: Unable to get database version number');
		return;
	}

	if ($version == DRDB_DB_VERSION) {
		$log->lwrite('database structure is up to date!');
		return;
	}
	
	$log->lwrite("Database structure needs to be updated");
		
	// Create 'hash_id' column in the main table and generate a uniqe hash for
	// all existing records
	if ($version < 3) {

		if (!DocRecord::addColumnHashId()){
			$log->lwrite('ERROR: Adding hash_id column failed, aborting upgrade');	
			return;
		}	

		$items = DocRecord::getRecordsOrderByID();
		
		echo '<br>';
		echo "Number of records to generate a hash_id for: " . sizeof($items);
		echo '<br>';
		
		foreach ($items as $item) {
		
			$item->generateHashId();
			$result = $item->editRecord();
		
			if ($result[0] != true) {
				$log->lwrite('ERROR: Unable to create hash_id for record: ' . $item->getId());
				return;
			}
		
		}
		
		// bump version number
		ProjectVersion::updateVersionNumber(3);
	}
  
	// Version 4 of the database adds a user permissions field to the 
	// user_config database table	
	if ($version < 4) {
	
	    if (UserConfigStorage::addPermissionsField()) {
	        
	        // Add existing statically defined permissions from user-permissions-config.php
	        // to the database
	        UserConfigStorage::populateInitialUsersAndPermissions();
	        
	        // bump version number
	        $version +=1;
	        ProjectVersion::updateVersionNumber($version);	        
	    }
	    else {
	        // Abort upgrade to version 4 and later
	        $log->lwrite('Unable to upgrade to version 4, aborting');
	        return;
	    }
	
	}

	 if ($version < 5) {
	     if (DatabaseConfigStorage::createInitialTable()) {

	         // bump version number
	         $version +=1;
	         ProjectVersion::updateVersionNumber($version);	         
	     }
	     else {
	         // Abort upgrade to version 4 and later
	         $log->lwrite('Unable to upgrade to version 5, aborting');
	         return;
	     }
	     
	 
	 }

	
	// for future changes use the following skeleton
	
	/*
	 if ($version < 6) {
	 
	 
	 // bump version number
	 $version +=1;
	 ProjectVersion::updateVersionNumber($version);
	 }
	 */

}
