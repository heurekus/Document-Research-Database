<?php

/**
 * html-tools.php
 *
 * This file contains a number of functions that are needed e.g. in 
 * bs_record_list_view.php to modify the HTML formatted text of database
 * fields
 *
 *
 * @version    1.0 2017-07-31
 * @package    DRDB
 * @copyright  Copyright (c) 2014-17 Martin Sauter
 * @license    GNU General Public License
 * @since      Since Release 1.0
 */


/**
 *
 * htmlRemoveAllInstancesOfEncapsTag()
 *
 * This function takes an input string, a search term and a tag (e.g. 'em' or
 * 'strong') that shall be removed before/after each search term. The function
 * then searches for ALL instances of the search term and then checks if there
 * are the correct tags before and after each search string. If so the tags are
 * removed.
 *
 * @param string: The input string
 * @param string: The search term
 * @param string: The tag to remove without the '<>' braces!
 * @param bool:   Is the search case sensitive (true) or in-sensitive (false)
 *
 * @return string: The modified string
 *
 */

function htmlRemoveAllInstancesOfEncapsTag($input_string, 
		                                   $search_term,
		                                   $tag_to_remove,
		                                   $case_sensitive_search) {
	
	$log = new Logging();
	
	$log->lwrite('input_string: ' . $input_string);
	$log->lwrite('search_term : ' . $search_term);
	$log->lwrite('tag_to_remo : ' . $tag_to_remove);
	$log->lwrite('$case_sens  : ' . $case_sensitive_search);		
	
	$search_offset = 0;
	do {
		
		if ($case_sensitive_search == false) {
			$mark_pos = stripos ($input_string, $search_term, $search_offset);			
			//$log->lwrite('XXXXX 2 CASE IN-sensitive search!');
		}
		else {
			$mark_pos = strpos ($input_string, $search_term, $search_offset);				
			//$log->lwrite('XXXXX 2 CASE SENSITIVE search!');
		}
				
		if ($mark_pos !== false){
			
			$log->lwrite('Found search term at position ' . $mark_pos);
	
			$modified = htmlRemoveSingleEncapsulatingTag($input_string, 
					                                     $mark_pos, 
					                                     strlen($search_term),
					                                     $tag_to_remove);

			// If the string was modified, modify the location of the search term
			if ($modified != false) {
				$mark_pos = $mark_pos - strlen('<' . $tag_to_remove . '>');	
			}
			
			// Look for another match in the string that comes later.
			// Start looking in the next iteration from the current 
			// search offset.
			$search_offset = $mark_pos + 1;
		}
	} while ($mark_pos !== false);	
	
	
    return $input_string;	
}		                                         



/**
 *
 * htmlRemoveEncapsulatingTag()
 *
 * This function removes the begin/end HTML tags for formatting such as 
 * italics or bold formatting in an HTML encoded string given as first paramter
 * before/after a search string at a given location with a given length. The 
 * tag name without <> encapsulation (e.g. 'em') is given to the function 
 * as the 4th parameter
 * 
 * The input string is taken by reference and is modified if html formating
 * codes are removed. True is returned in this case. 
 * 
 * If no html formatting codes are found the input string is not modified and 
 * false is returned as the function's result parameter.
 *
 * Note: The calling function is interested if the string was modified or not
 * as the $loc_start/$loc_end will now come earlier in the string. If the
 * calling function then searches for another $loc_start/$loc_end before/after
 * which html formatting codes are removed this change has to be taken into
 * account.
 *
 * @param &string: the input string (by reference, modifications done here!)
 * @param int: start location of the search term, start tag removed before
 * @param int: length of the search term, end tag removed after
 * @param string: the tag name to be removed without the parantheses' ('<>')
 *
 * @return boolen: true if modified, else false
 *
 */

function htmlRemoveSingleEncapsulatingTag(&$str, $loc_start, $len_search_str, $tag) {

	$log = new Logging();
	$log->lwrite('String before modification: ' . $str);
	
	// ======================================================
	// SEARCH for html tags BEFORE the search string
	// ======================================================
	
	// find LAST tag BEGIN before the search term	
	$truncated = substr($str, 0, $loc_start);
	$pos_tag_begin_before = strripos ($truncated, '<' . $tag . '>');
	
	if ($pos_tag_begin_before === false) {
		
		$log->lwrite('No <tag> before search string, nothing to remove!');
		return(false);
	}
		
	// find LAST tag END before the search term
	$pos_tag_end_before = strripos($truncated, '</' . $tag . '>');
	
	// If there is no end marker before the search string that is fine
	if ($pos_tag_end_before === false) {
		$pos_tag_end_before = 0;
		
		$log->lwrite('No </tag> before search string, that is fine');
	}
	
	$log->lwrite('<tag> before: ' . $pos_tag_begin_before .
			    ' </tag> before: ' . $pos_tag_end_before);
	
	// if the last END comes AFTER the BEGIN the search term is NOT enclosed.
	if ($pos_tag_end_before > $pos_tag_begin_before) {
		$log->lwrite('</tag> before <tag> BEFORE search string, no replacement!');
		return false;
	} 

	// ======================================================
	// SEARCH for html tags AFTER the search string
	// ======================================================
	
	// find the FIRST tag BEGIN AFTER the search term
	$pos_tag_begin_after = stripos ($str, '<' . $tag . '>', $loc_start + $len_search_str);
	
	// If there is no <tag> after the search string that's o.k.
	if ($pos_tag_begin_after === false) {
		$pos_tag_begin_after = 0;
		$log->lwrite('No <tag> after the search string, that is o.k.!');
	}
	
	// find the FIRST tag END AFTER the search term
	$pos_tag_end_after = stripos ($str, '</' . $tag . '>', $loc_start + $len_search_str);

	// If there is no end marker there is a problem, abort!
	if ($pos_tag_end_after === false) {
		$log->lwrite('No </tag> after search string, Error, ABORT!');
		return (false);
	}
		
	// If the begin tag exists and is before the after, the search term 
	// was not enclosed by the html tag
	if ($pos_tag_begin_after > 0) {
		if ($pos_tag_begin_after < $pos_tag_end_after) {
			$log->lwrite('<tag> before </tag> AFTER search string, Error, ABORT!');
			return false;
		}
	}

	$log->lwrite('</tag> after: ' . $pos_tag_end_after .
	    		 ', <tag> after: ' . $pos_tag_begin_after);
	
	// Remove the tag start and end term encapsulating the search string
	$str = substr_replace($str, '', $pos_tag_end_after, strlen('</' . $tag . '>'));
	$str = substr_replace($str, '', $pos_tag_begin_before, strlen('<' . $tag . '>'));
	
    $log->lwrite('Modified string: ' . $str);
    
    return true;
}
