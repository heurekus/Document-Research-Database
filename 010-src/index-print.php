<?php
/**
 * index.php
 *
 * Main file
 *
 * @version    1.0 2014-02-01
 * @package    DRDB
 * @copyright  Copyright (c) 2014 Martin Sauter
 * @license    GNU General Public License
 * @since      Since Release 1.0
 */

// Before going any further check if the web server has authenticated a user.
if (!isset($_SERVER['PHP_AUTH_USER'])) {
	echo '<p>ERROR: No user authentication configured e.g. in apache2.conf ' .
			'or .htaccess. Aborting...</p>';
	exit;
}

require_once 'includes/init.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title><?php echo DatabaseConfigStorage::getCfgParam('title');?></title>
<link href="css/main.css" rel="stylesheet" type="text/css" />
</head>

<body>

  
  <div class="content">
    <?php 
    
    $executionStartTime = microtime(true);
    loadContent('content', 'home');
    $executionEndTime = microtime(true);

    $log = new Logging();
    $executionTime = round($executionEndTime - $executionStartTime, 4);
    $log->lwrite("Execution Time: " . $executionTime . ' seconds');
    
    ?>
  </div><!-- end content -->
  

</body>
</html>
