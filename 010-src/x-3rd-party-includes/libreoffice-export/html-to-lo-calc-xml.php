<?php
/**
 * html-to-lo-calc-xml.php
 *
 * This class extends html-to-lo-xml class and adds additional functionality 
 * that is not needed for for writer documents to convert HTML formatted 
 * string to Libreoffice CALC XML output.
 * 
 * The class then needs to be extended again to write the result it generates
 * into a CALC template file.
 *
 * @version    1.0 2019-01-13
 * @package    libreoffice-export
 * @copyright  Copyright (c) 2014-19 Martin Sauter
 * @license    GNU General Public License
 * @since      Since Release 1.0
 *
 */

require_once 'lo-export-log.php';
require_once 'html-to-lo-xml.php';

/**
 * class Html_To_LibreOffice_Calc_XML
 *
 */

class Html_To_LibreOffice_Calc_XML extends Html_To_LibreOffice_XML {

public function Convert_HTML_To_LO_Calc_Cell_Content($html_str) {
   
    $log_buf = "";
    
    $this->xml_buf .= '<table:table-cell office:value-type="string" ' . 
                      'calcext:value-type="string">';
    $log_buf = $this->Convert_Html_To_LO_XML ($html_str, "", false);
    $this->xml_buf .= '</table:table-cell>' . "\n";

    return $log_buf;
}

public function New_Table_Row() {
    $this->xml_buf .= '<table:table-row table:style-name="ro1">';
}

public function End_Of_Table_Row() {
    $this->xml_buf .= '</table:table-row>' . "\n";
}

} // end of class
    