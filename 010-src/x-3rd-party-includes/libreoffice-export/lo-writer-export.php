<?php

/**
 * lo-writer-export.php
 *
 * The class in this file extends the Html_ToLibreOffice_XML functionality
 * and contains the code to save text and style information to a 
 * Libreoffice Writer template document.
 * 
 * IMPORTANT: This class requires the 'php-zip' module to be installed
 *            which might have to be installed on the server separately from
 *            the standard repositories (e.g. apt install php-zip)
 *
 * @version    1.1 2019-01-24
 * @package    libreoffice-export
 * @copyright  Copyright (c) 2014-2019 Martin Sauter
 * @license    GNU General Public License
 * @since      Since Release 1.0
 */

require_once 'lo-export-log.php';
require_once 'html-to-lo-xml.php';

class LO_Writer_Export extends Html_To_LibreOffice_XML {

/**
 * Save_To_Writer_Document()
 *
 *
 * @param string $src Path and filename to the template Libreoffice Writer document
 * @param string $dst Path and filename to which the output Writer document is to 
 *               be written to.
 *
 * @return string Log information for debugging
 */	
	
public function Save_To_Writer_Document ($src, $dst) {

	$log = "";
	
	if (!copy($src, $dst)) {
		LoxLog::add($log, "ERROR: Unable to copy file, aborting");
		return $log;
	}
	
	$zip = new ZipArchive;
	$res = $zip->open($dst);
	if ($res !== TRUE) {
		LoxLog::add($log, "ERROR: Couldn't open writer template copy");
		return $log;
	}
	
	LoxLog::add($log, "Writer template open successfull");
	
	$content = $zip->getFromName('content.xml');

	$xml_styles = $this->Get_XML_Page_Break_Para_Styles();
	$xml_styles .= $this->Get_XML_Style_Description();
		
	$content = str_replace("</office:automatic-styles>", 
			               $xml_styles . "</office:automatic-styles>", 
						   $content);
	
	$content = str_replace("</office:text>", 
			               $this->xml_buf . "</office:text>", $content);
		
	$zip->addFromString("content.xml", $content);
	$zip->close();
		
	return $log;
} 
	
}