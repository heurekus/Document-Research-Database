<?php
/**
 * lo-export-log.php
 *
 * This class implements logging functionality for the Libreoffice Export
 * library.
 *
 * @version    1.1 2019-01-14
 * @package    libreoffice-export
 * @copyright  Copyright (c) 2019 Martin Sauter
 * @license    GNU General Public License
 * @since      Since Release 1.0
 */


class LoxLog {
	
	// Set this constant to true to log everything or to false to only
	// log error messages
	const LOG_ALL = false;
	
	/**
	 * add()
	 *
	 * Write $message to $buffer and prepend with date, time, ip and user 
	 * information. Depending on LOG_ALL being set to true or false, 
	 * all debug information or only error debug inforomation is 
	 * put into the return buffer.
	 * 
	 * Only set LOG_ALL to true for in-depth debugging as output is very 
	 * extensive if all debug info is recorded.
	 * 
	 * Note: This is a static method, so no object instantiation is required.
	 *
	 * @param string $buffer The buffer to write the message into. Called by
	 *               references
	 * @param string $message The message
	 * @param boolean $is_error True if this is an error log, false by default
	 *
	 */
	
    public static function add(&$buffer, $message, $is_error = false) {            

    	if (!self::LOG_ALL && !$is_error) {
    		return;
    	}
    	
        // define current time and suppress E_WARNING if using the system 
        // TZ settings (don't forget to set the INI setting date.timezone)
        $time = @date('[Y/m/d H:i:s]');        
        $client_ip = $_SERVER['REMOTE_ADDR'];        
        $user = $_SERVER['PHP_AUTH_USER'];
        
        // Get the name of the calling function for the debug output
        // Note: if the code is not from "within" a function (e.g. just
        // a php file that executes code, "incluce()" is shown as
        // calling function.
        $dbt=debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS,2);
        $caller = isset($dbt[1]['function']) ? 
                        $dbt[1]['function'] : null;

        $buffer = $buffer . $time . " " . 
                  $client_ip . " " . 
                  $user . " " . 
                  $caller . "() " .  
                  $message . PHP_EOL;
                  
        return;
    }
        
}