<?php
/**
 * change_record_id.php
 *
 * This module is called via a JSON request from bs_record_details.php
 * when the user wants to move a record to a different category. The module
 * expects parameters in the HTTP post in JSON format and returns the result
 * in JSON format as well.
 *
 * @version    1.0 2018-10-21
 * @package    DRDB
 * @copyright  Copyright (c) 2018 Martin sauter
 * @license    GNU General Public License
 * @since      Since Release 1.0
 */

$log = new Logging();
$log->lwrite('change_record_id.php: Attempting to change the record\'s ID...');

// Prepare a 0 return value for the new ID that signals an error in case
// the id of the record could not be changed. 
$return_vars['new_id'] = 0;
$return_vars['error_str'] = 'Unable to move the record! Check log for details';
$return_vars_JSON = json_encode($return_vars);

// Web security check
if (checkAntiCsrfProtection() == false) {
	$log->lwrite('change_record_id.php: security error, exiting');
	exit;
}

// If the user has no edit rights, don't change the record id
if (!UserPermissions::hasAccess('edit')) {
    $log->lwrite('change_record_id.php: User has no editing rights, aborting!');
    echo $return_vars_JSON;
    return;
}

// Check if the required HTTP POST input parameters are present
if (!isset($_POST['start_id'])) {
	$log->lwrite('start_id not present, unable to change the record id');
	echo $return_vars_JSON;
	return;
}

if (!isset($_POST['current_id'])) {
	$log->lwrite('current_id not present, unable to change the record id');
	echo $return_vars_JSON;	
	return;
}

// Read the parameters and check if they make sense
$start_id = (int) $_POST['start_id'];
$log->lwrite('start_id: ' . $start_id);

$current_id =  (int) $_POST['current_id'];
$log->lwrite('current_id: ' . $current_id);

if ($start_id <= 1000) {
	$log->lwrite('category_id value is too low, aborting');
	echo $return_vars_JSON;	
	return;
}

if ($current_id <= 1000) {
	$log->lwrite('current_id value is too low, aborting');
	echo $return_vars_JSON;	
	return;
}

// Check and abort if the user has locked this record for editing in another
// tab. Note: If the record is locked by another user, the moveRecord()
// function will detect this and abort.
$is_locked = isRecordStillLockedForThisUser($current_id, false);
if ($is_locked[0]) {
	$log->lwrite('Aborting: Record is locked by the user, perhaps in another tab.');
	$return_vars['error_str'] = 'Unable to move the record! The record is ' . 
	                            'currently edited in this or another tab.';
	$return_vars_JSON = json_encode($return_vars);
	echo $return_vars_JSON;
	return;
}

// Run the move procedure
$new_id = calculateNewRecordIdAtEndOfCategory();
$log->lwrite('New ID for the record: ' . $new_id);
$move_result = moveRecord($current_id, $new_id); 

if (!$move_result[0]){
	$log->lwrite('Unable to move the record to the new id, aborting');
	$log->lwrite($move_result[1]);
	$return_vars['error_str'] = $move_result[1];
	$return_vars_JSON = json_encode($return_vars);
	echo $return_vars_JSON;
	return;	
}

// Return the result as JSON encoded array
$return_vars['new_id'] = $new_id;
$return_vars['error_str'] = 'no error';
$return_vars_JSON = json_encode($return_vars);
echo $return_vars_JSON;
return; 

?>

