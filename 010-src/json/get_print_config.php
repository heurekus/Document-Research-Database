<?php
/**
 * get_print_config.php
 *
 * This module is called from Javascript functionality on the client side to
 * get the current print/export to Libreoffice configuration settings.
 * These were previously loaded from the user configuration database table 
 * into session variables.
 * 
 *
 * @version    1.0 2017-04-20
 * @package    DRDB
 * @copyright  Copyright (c) 2017 Martin sauter
 * @license    GNU General Public License
 * @since      Since Release 1.0
 */

$had_to_init_vars = false;

$log = new Logging();
$log->lwrite('get_print_config.php: Sending printing configuration to client');

// Web security check
if (checkAntiCsrfProtection() == false) {
	$log->lwrite("get_print_config.php: security error, exiting");
	exit;
}
	

// Loop through all allowed variable names to see if they exist. If they
// do exist, put true/false into the corresponding session variables.

$allowed_print_variable_names = array ("showId", "includeLink", "pageBreak");

foreach ($allowed_print_variable_names as $name) {

	if (isset($_SESSION['blt_user_' . $name])) {
	
		$print_settings[$name] =  $_SESSION['blt_user_' . $name] ? true : false;
		$log->lwrite('get_print_config.php: setting "' . $name . '" to: ' .
				$print_settings[$name]);
	}
	else {
		// The print config variable does not exist, so initialize it 
		// to a default value
		$log->lwrite('get_print_config.php: setting "' . $name . '" to ' . 
				     'DEFAULT = true');
		$print_settings[$name] = true;	

		// In addition, create and initialize the varialbe on the server side
		$log->lwrite('creating session variable ' . 'blt_user_' . $name);
		$_SESSION['blt_user_' . $name] = true;
		$had_to_init_vars = true;
		
	} 
			
}

// If print variables were not yet present and were initialized to the
// default value, store them in the database
if ($had_to_init_vars == true) {
	$log->lwrite('saving newly initialized session variables');
	UserConfigStorage::saveUserConfig();
}

// Convert printer settings to JSON and send to client in the body of the
// document.

$print_settings_JSON = json_encode($print_settings);

echo $print_settings_JSON;

?>






