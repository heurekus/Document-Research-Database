/**
 * user_management.js
 * 
 * Javascript routinges for
 *  
 *     bs_user_management.php
 *     bs_user_add.php
 *     
 * These pages have similar and only minimal Javascript requirements, 
 * so a single source file can handle them both.
 *

 * @version    1.0 2021 04 08
 * @package    DRDB
 * @copyright  Copyright (c) 2021 Martin Sauter
 * @license    GNU General Public License
 * @since      Since Release 1.0
 */

/**
 * windowClose()
 *
 *
 * @param none
 *
 * @return none
 */

function windowClose() {
	window.close();
}


function showChangePwdDialog(event) {

	// Copy the username for which the password is to be changed from the button
	// info object to a global variable. We need it later once the user presses
	// the 'change' button in the modal dialog box.
	username = event.target.attributes.user.nodeValue;
	
	// Set text in the modal dialog box
	$('.modal-title').html('<strong>Change Password for user ' + username + '</strong>');
	
	// Add the username as a hidden form element so it goes back to the server when the
	// User presses the 'change' button.
	$('#pwdchange').append('<input type="hidden" name="username" value="' + username + '" />');
	
	// And show it!
	$('#pwdChangeModal').modal('show');
}

function showDeleteUserDialog(event) {

	// Copy the username for which the password is to be changed from the button
	// info object to a global variable. We need it later once the user presses
	// the 'change' button in the modal dialog box.
	username = event.target.attributes.user.nodeValue;
	
	// Set text in the modal dialog box
	$('.modal-title-del-user').html('<strong>Delete user ' + username + '</strong>');
	
	// Add the username as a hidden form element so it goes back to the server when the
	// User presses the 'change' button.
	$('#deluser').append('<input type="hidden" name="username" value="' + username + '" />');
	
	// And show it!
	$('#userDeleteModal').modal('show');
}


/**
 *
 * @param NA
 *
 * @return NA
 * 
 */

$(document).ready(function(){
    // Add event listeners for left column menu buttons
	$('#closetab').click(windowClose);
	
    // Add event listeners for buttons
	// Note: jquery uses '.' for CLASSES and '#' for IDs.
	$('.password-user').click(showChangePwdDialog);
	$('.del-user').click(showDeleteUserDialog);
	
});
