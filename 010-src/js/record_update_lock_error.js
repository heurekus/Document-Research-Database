/**
 * record_update_lock_error.js
 *
 * Shows the modal dialog box to inform the user that the record is locked.
 *
 * @version    2.0 2018 10 28
 * @package    DRDB
 * @copyright  Copyright (c) 2018 Martin Sauter
 * @license    GNU General Public License
 * @since      Since Release 1.0
 */

// Get the reason why locking the record has failed and put it into
// the modal dialog box.
var lock_result = $('#lock_result').html();
$('#lock-failure-text').html(lock_result);

// And now show the modal dialog box.
$('#lockFailDocExecModal').modal('show');
