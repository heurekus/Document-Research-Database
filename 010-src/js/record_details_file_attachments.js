/**
 * record_details_handle_file_attachments.js
 *
 * This module contains client side Javascript code that is included
 * by bs_record_details.php to show the file upload dialog box. 
 * The functions directly interact with the HTML code generated by this 
 * php module.
 *
 * @version    1.0 2022 06 06
 * @package    DRDB
 * @copyright  Copyright (c) 2022 Martin Sauter
 * @license    GNU General Public License
 * @since      Since Release 1.0
 */


/**
 * showUploadFilesModal()
 *
 * This function is called from the record details page when the user 
 * clicks on a file upload button. It then gets the name of the database
 * field to which the files should be attached from the "id" element of 
 * the button and puts it into a hidden form parameter in the upload files
 * modal dialog box.
 *
 * @param element
 *
 * @return NA
 */

function showUploadFilesModal (element) {

	// Get the field name. slice(6) remvoes the first six chars at the
	// beginning (field_)
	dbFieldNameForFileUpload = this.getAttribute("id").slice(6);

	// Now set the field name in the the file upload modal dialog box
	document.getElementById("file_upload_field_name").value = dbFieldNameForFileUpload;

    // Put the anti-CSRF token in a hidden element
    var token = document.getElementById("doc_token").innerHTML;
	document.getElementById('file_upload_token').value = token;

	// Clear filenames that might have been selected earlier
	document.getElementById("filename").value = "";


	$('#uploadFilesModal').modal('show');
}



/**
 * deleteFileDblClick()
 *
 * 
 * @param none
 *
 * @return none
 * 
 */

 function deleteFileDblClick() {
     
    var field_name = this.getAttribute("del_field_name");
    var file_name = this.getAttribute("del_file_name");

    console.log("In delete file dbl-click handler 2!");

	// Put values into the invisible html form
	document.getElementById('del_field_name').value = field_name;
	document.getElementById('del_file_name').value = file_name;
	
	// Put the anti-CSRF token in a hidden element
	var token = document.getElementById("doc_token").innerHTML;
	document.getElementById('del_file_token').value = token;

    // And POST the form
	var delFileRemoveForm = document.getElementById('del-file-attachment');    
	delFileRemoveForm.submit();
         
}


/**
*
* @param NA
*
* @return NA
* 
*/

$(document).ready(function(){

	// Add event listeners
	$('.upload-files').click(showUploadFilesModal);

	// Handler for even 'delete a file attachment'
	$('.delete-file').dblclick(deleteFileDblClick);

});
