/**
 * record_list_view_move.js
 *
 * The functions and variables below are used by the 'move and insert a document' 
 * functionality on the record_list_view.php page.
 *
 * @version    2.0 2018 10 27
 * @package    DRDB
 * @copyright  Copyright (c) 2018 Martin Sauter
 * @license    GNU General Public License
 * @since      Since Release 1.0
 */

var moveId = 0;
var moveDocName = "";
var insertAfterId = 0;
var insertAfterDocName = "";

function showMoveButtons() {
	$(".ms-insert-after").hide();
	$(".ms-move").show();
}

function moveClicked() {
	console.log('moveClicked(): ' + this.getAttribute("id"));
	moveId = this.getAttribute("id");
	moveDocName = this.getAttribute("docName");
	$(".ms-move").hide();
	$(".ms-insert-after").show();

	// Hide the insert-after button for the current database record
	// as one can't insert after itself.
	//
	// Note: This jquery expression chains the class name and 
	// parameter id + value to only act on a single element instead
	// of all elements of that class!
	$('.ms-insert-after[id="'+ moveId + '"]').hide();
}

function insertAfterClicked() {
	insertAfterId = this.getAttribute("id");
	insertAfterDocName = this.getAttribute("docName");

	console.log('insertAfterClicked() A: ' + moveId + ' ' + insertAfterId);
	console.log('insertAfterClicked() A: ' + moveDocName + ' ' 
			                               + insertAfterDocName);	

	// if the document variables are empty, put something inside
	if (moveDocName.length < 2) {
		moveDocName = '----';
	}		

	if (insertAfterDocName.length < 2) {
		insertAfterDocName = '----';
	}		
	
	$(".ms-insert-after").hide();	

	console.log('insertAfterClicked() B: ' + moveId + ' ' + insertAfterId);
	console.log('insertAfterClicked() B: ' + moveDocName + ' ' 
			                               + insertAfterDocName);

    // Put the database IDs of move/insert-after into the modal dialog
    // POST form.
    $('input[name="doc-move"]').val(moveId);   
    $('input[name="doc-after"]').val(insertAfterId);
    	
	// Assemble move/insert text for the modal dialog box
	$("#modal-replace-text").html('<p>Insert <strong>\'' + 
			                      moveDocName + 
			                      '\'</strong> after <strong>\'' + 
			                      insertAfterDocName + 
			                      '\'</strong>?');
	
	// Now show the CONFIRM/cancel modal dialog box to let the user
    // confirm the move/insert operation
	$('#myModal').modal('show')
}


/**
*
* @param none
*
* @return none
*/
	
$(document).ready(function(){

    // Add event listeners
	// Note: jquery uses '.' for CLASSES and '#' for IDs.	
	$('#show-move-buttons').click(showMoveButtons);
	$('.ms-move').click(moveClicked);
	$('.ms-insert-after').click(insertAfterClicked);	

});


