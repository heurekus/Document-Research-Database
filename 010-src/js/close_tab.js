/**
 * home_init.js
 *
 * Contains Javascript functionality to close the current tab and is used on
 * quite a number of pages. Having this funcionality in an external JS script
 * is necessary as Content Security does not allow inline JS code.
 *
 * @version    2.0 2018 11 20
 * @package    DRDB
 * @copyright  Copyright (c) 2018 Martin Sauter
 * @license    GNU General Public License
 * @since      Since Release 1.0
 */


/**
 * windowClose()
 *
 * Close the currently open tab.
 *
 * @param none
 *
 * @return none
 */

function windowClose() {
	window.close();
}

/**
*
* Handler to close the window
*
* @param none
*
* @return none
*/
	
$(document).ready(function(){
	
	// Note: jquery uses '.' for CLASSES and '#' for IDs.
	
    // Add event listeners
	$('.close-tab').click(windowClose);

});
