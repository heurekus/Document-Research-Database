/**
 * record_details_keys_and_swipe.js
 *
 * Javascript functionality to handle key presses and swipe events 
 * for bs_record_details.php
 *
 * @version    2.0 2018 10 23
 * @package    DRDB
 * @copyright  Copyright (c) 2018 Martin Sauter
 * @license    GNU General Public License
 * @since      Since Release 1.0
 */

// ==========================================================================
//
// Keyboard functions
//
// ==========================================================================
	
//Catch keyboard events to go to the next or the previous document when
//the arrow right / arrow left key is pressed
document.onkeydown = checkKey;

function checkKey(e) {

	if (document.getElementById("is_convention_record").innerHTML == 'true') return;
	
	e = e || window.event;
	
	if (e.keyCode == '37') {
	   // left arrow
	   dbDocId = document.getElementById("doc_db_id").innerHTML;
	   window.location.href = "index.php?content=bs_record_details&id=" + 
	                          dbDocId + "&previous_record=1";
	}
	else if (e.keyCode == '39') {
	   // right arrow
	   dbDocId = document.getElementById("doc_db_id").innerHTML;
	   window.location.href = "index.php?content=bs_record_details&id=" + 
	                           dbDocId + "&next_record=1";
	}

}


// ==========================================================================
// The following code deals with mobile and touch screens. Some functionality
// is only activated on touch devices
// ==========================================================================

//The following variable, the if statement and the function handle influence
//if an editor is opened when the user clicks once or tabs with his finger
//on text of a database record. On Android, iPhones and iPad the functionality
//is enabled as those OSes are used on touch based devices. 
//If those OS names are not included in the user agent, the functionality 
//is diasabled as users often click on text without the intention to start
//an editor. Hence, only double click events trigger the edit mode.

isMobile = false;
//Note: /xxx/i is a regular expression, '.test' tests for true or
//false against the variable in the round brakets (userAgent...)
if (/Android|iPhone|iPad|iPod/i.test(navigator.userAgent)) isMobile = true;

function editDocClickField() {

	if (isMobile == true) {
		// On mobile, click acts like double click.
		// NOTE: editDocDblClickField is called with '.call(this)' which
		// forwards the calling object into the object space of
		// editDocDblClickField. If editDocDblClickField() is called,
		// 'this' is undefined and the function can't access the part of
		// the DOM from which it was called.
		editDocDblClickField.call(this);
	} 
	// else ignore the single click call, the user agent is not from a 
	// mobile OS	
}

//
//Activate swipe left / right functionality if we are on a touch screen device
//Part of jquery.touchSwipe.min.js
//
// Note: Called from the document ready function in record_details_load_unload.js
//

function activateSwipeIfOnMobile()
{
	if (isMobile != true) return;

	if (document.getElementById("is_convention_record").innerHTML == 'true') return;

	$(function() {      
	    //Keep track of how many swipes
	    var count=0;
	    //Enable swiping...
	    $("#swipe").swipe( {
	      //Single swipe handler for left swipes
	      swipeLeft:function(event, direction, distance, duration, fingerCount) {
	          dbDocId = document.getElementById("doc_db_id").innerHTML;
	    	  window.location.href = "index.php?content=bs_record_details&id=" + 
	    	                           dbDocId + "&next_record=1" +
	    	                           "#start_after_swipe";  
	      },
	      
	      swipeRight:function(event, direction, distance, duration, fingerCount) {
	    	  dbDocId = document.getElementById("doc_db_id").innerHTML;
	    	  window.location.href = "index.php?content=bs_record_details&id=" + 
	    	                            dbDocId + "&previous_record=1" +
	    	                            "#start_after_swipe"; 
	      }
	    });
	  });
}
