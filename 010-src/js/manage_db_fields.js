/**
 * manage_db_fields.js
 *
 * This module contains client side Javascript code that is included
 * by bs_manage_db_fields.php enable admin users to manage fields
 * of the main database table.
 *
 * @version    2.0 2021 06 01
 * @package    DRDB
 * @copyright  Copyright (c) 2018-2021 Martin Sauter
 * @license    GNU General Public License
 * @since      Since Release 1.0
 */

// ==========================================================================
// The following variable, the if statement and the function handle influence
// if a CLICK or DOUBLECLICK is required to activate / deactivate a field. 
// On Android, iPhones and iPad the single click functionality is enabled
// as those OSes are used on touch based devices where dbl clicks don't work.
// ==========================================================================


// module global variable for change field name functionality 
fieldName = '';

isMobile = false;

// Note: /xxx/i is a regular expression, '.test' tests for true or
// false against the variable in the round brakets (userAgent...)
if (/Android|iPhone|iPad|iPod/i.test(navigator.userAgent)) isMobile = true;

function handleSingleClick() {

	if (isMobile == true) {
		
		// On mobile, click acts like double click.
		// NOTE: toggleVisibility is called with '.call(this)' which
		// forwards the calling object into the object space of
		// toggleVisibility. If toggleVisibility() is called,
		// 'this' is undefined and the function can't access 
		// the part of the DOM from which it was called.
		console.log('TODO: Handle Single Click Event');
		
		handleDoubleClick.call(this);
	} 
	// else ignore the single click call, the user agent is not from a 
	// mobile OS	
}



/**
 * handleDoubleClick()
 *
 * This function is called when the user double clicks on a panel
 * to edit the name.
 *
 * @param element
 *
 * @return none
 */

function handleDoubleClick() {
	
	// Get current field name of panel that the user has double-clicked on
	// Initially the content of the id tag was used. However, the 'reorder'
	// function below takes the text element (which does not have a problem
	// with double quotes). So to use the same fieldName retrieval
	// mechanims everywhere in this module, also take the content of the
	// text part here.
	
	// fieldName = this.id;	
	fieldName = $("#panel-id-print-name", this).text();
	console.log(fieldName);
	
	// Put the field name the user might want to change into the input box.
	// Also put the original name in a hidden field so the server handler
	// function know which field to change
	$("#id-input-field-database-field-name").val(fieldName);
	$("#id-original-field-name").val(fieldName);

	// Get export inline print setting for this field and set the checkbox accordingly
	inlinePrint = $("#panel-id-f-inline-print", this).text();
	console.log(inlinePrint);
	
	$("#id-inline-print").prop('checked', false);
	if (inlinePrint == "1") {
		$("#id-inline-print").prop('checked', true);
	}
	
	// Now show the CONFIRM/cancel modal dialog box to let the user
    // confirm the move/insert operation
	$('#myRenameModal').modal('show')
}


/**
 * handleClickConfirmButtonRenameField()
 *
 * When the user presses the confirm button, just call 
 * submit() for the field name change form. 
 *
 * @param none
 *
 * @return none
 */

function handleClickConfirmButtonRenameField() {
	
	console.log("Confirm button for rename field pressed");
	$('#rename-field-name-form').submit();
	
}


/**
 * handleClickDeleteFieldButton()
 *
 * When the user presses the delete button, just call 
 * submit() for the field delete form. 
 *
 * @param none
 *
 * @return none
 */

function handleClickDeleteFieldButton() {
	
	console.log("Confirm button for deleting a field pressed");
	$('#delete-field-form').submit();
	
}



/**
 * handleClickConfirmButtonRenameField()
 *
 * When the user presses the confirm button, just call 
 * submit() for the field name change form. 
 *
 * @param none
 *
 * @return none
 */

function handleClickConfirmButtonNewField() {
	
	console.log("Confirm button or new field creation pressed");
	$('#new-field-name-form').submit();
	
}

/**
 * handleCreateNewFieldButton()
 *
 * When the user presses the Create New Field button, show 
 * the modal dialog box
 *
 * @param none
 *
 * @return none
 */

function handleCreateNewFieldButton() {
	$('#myAddFieldModal').modal('show');
}

/**
 * handleDeleteFieldButton()
 *
 * When the user presses the Delete Field button, show 
 * the modal dialog box
 *
 * @param none
 *
 * @return none
 */

function handleDeleteFieldButton() {
	$('#myDeleteFieldModal').modal('show');
}


/**
 * This jQuery function construct implements the draggable panels
 *
 * @param none
 *
 * @return none
 */

jQuery(function($) {
    var panelList = $('#draggablePanelList');

    panelList.sortable({
        // Only make the .panel-heading child elements support dragging.
        // Omit this to make then entire <li>...</li> draggable.
        handle: '.panel-heading', 
        update: function(event, ui) {
        	updateFieldOrder();        	
        } // end of update
    }); // end of panelList.sortable
}); // end of jQuery function


/**
 * updateFieldOrder()
 *
 * When some aspect of the field list is changed this function is called
 * It reads the current configuration of the field list from the DOM,
 * assembles POST variables and then sends a request to the server with
 * the updated data.
 *
 * @param none
 *
 * @return none
 */

function updateFieldOrder(){
	console.log('\r\n\r\nReordering!');

	// delete the old field strings
	var fieldNameString = "";
	var fieldPrintNameString = "";
	var fieldVisibilityString = "";
	var fieldInlinePrintString = "";
	
	var fieldData = {};	
	
	// Loop over each panel in the new sort order!
    $('.doc-field').each(function(index, element) {

    	fieldName = $("#panel-id-field-name", element).text();
    	fieldPrintName = $("#panel-id-print-name", element).text();
    	fieldInlinePrint = $("#panel-id-f-inline-print", element).text();
        	            	
    	// Add the field if the name is not 0 (list elements in the
    	// sidebar are going through this loop as well but return
    	// a 0-length string as they don't have an id = panel-id!
	    if (fieldName.length != 0) {
        
	        var valueArray = [fieldName, "1", fieldInlinePrint];	        
	        fieldData[fieldPrintName] = valueArray;
	        
	    } // end of fieldName.length != 0
    
    }); // end of the 'each' list element loop


    fieldDataJson = JSON.stringify(fieldData);
    console.log(fieldDataJson);
    
	// HTTP post query to the server to update the field struct, including the 
	// security token as a HTTP POST parameter.
	var dbDocToken = document.getElementById("doc_token").innerHTML
	
	
	$.post("index.php?content=update_main_table_field_order",
	       {fieldDataJson: fieldDataJson,
	        token: dbDocToken}, 
	        updateFieldOrderCallback);
	  	    
}


/**
 * updateFieldOrderCallback()
 *
 * This function gets called once the AJAX call update the in which order 
 * to display a records's filed returns. It maks a log to the console and
 * otherwise does nothing if the call was successful. If the server could
 * not be reached an error pop-up is presented.
 *
 * @param data, status
 *
 * @return NA
 */

function updateFieldOrderCallback(data, status) {

    var resultText = "";
	
    if (status == "success") {
  
		console.log('Field sort order successfully updated');
    }
    else {
        alert ('problem sending update to server!');
    }

}

/**
 * windowClose()
 *
 *
 * @param none
 *
 * @return none
 */

function windowClose() {
	window.close();
}

/**
 *
 * Misc stuff that needs to be done once the page is fully loaded
 *
 * @param none
 *
 * @return none
 */
	
$(document).ready(function(){

	// In mobile touch browsers, other input options are available. Change
	// info text accordingly.
	if (isMobile == true) {
		$('#info-text').html('Usage:<br>1. Drag/drop the fields into the ' + 
				             'desired order.<br> ' + 
				             '2. Tap on field name to edit the name');
	}

	// Add event listeners for click and double clicks of field buttons that
	// are of class 'doc-field'.
    $('.doc-field').click(handleSingleClick);
    $('.doc-field').dblclick(handleDoubleClick);
    
    // Add event listener for submit of changed field name in modal dialog box
    $('#confirm-button-rename-field').click(handleClickConfirmButtonRenameField);
    
    // Add event listener for submit of 'create new field' modal dialog box
    $('#confirm-button-add-field').click(handleClickConfirmButtonNewField);
    
    // Add event listener for delete button of 'delete' modal dialog box
    $('#delete-field-button').click(handleClickDeleteFieldButton);

    
    // Add event listeners for left column menu buttons
	$('#button-create-new-field').click(handleCreateNewFieldButton);
    $('#button-delete-field').click(handleDeleteFieldButton);
	$('#closetab').click(windowClose);
		
	
});

