/**
 * record_update_ckeditor.js
 *
 * Javascript code to replace a standard text input box with CKEDITOR
 *
 * @version    2.0 2018 10 27
 * @package    DRDB
 * @copyright  Copyright (c) 2018 Martin Sauter
 * @license    GNU General Public License
 * @since      Since Release 1.0
 */

// After the document has fully loaded, replace all text areas with CKEDITOR
$(document).ready(function(){
	
	// Loop over each input field and replace it
    $('.drdb_input').each(function(index, element) {
    	var db_field_name = $(element).attr('id');
    	var editor = $(element).attr('editor');
    	replace_input_field_with_ckeditor(db_field_name, editor);
    });
});

function replace_input_field_with_ckeditor(db_field_name , config_file){
	
	CKEDITOR.replace(db_field_name, {height: 100,  customConfig: config_file});
}

