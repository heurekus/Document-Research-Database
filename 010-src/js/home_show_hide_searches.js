/**
 * home_show_hide_searches.js
 *
 * Shows and hides the different search options in bs_home.php
 *
 * @version    2.0 2018 10 23
 * @package    DRDB
 * @copyright  Copyright (c) 2018 Martin Sauter
 * @license    GNU General Public License
 * @since      Since Release 1.0
 */


/**
 * 
 * docShowOption(name)
 *
 * Makes all optional HTML invisible in the main column and activates the 
 * optional HTML part that is surrounded by a <div> field with an 'id' = name
 *
 * @param event Note: The variable contains the name of the search/list 
 *              option to show) as 'event.data.param1'. This is indirection
 *              is required as the function is not called directly but 
 *              is registed in a JQuery statement as a click handler. As
 *              a consequence, direct input paramters are not possible.
 *
 * @return none
 * 
 */

function docShowOption(event) {

	// The first parameter contains a list input parameters to the function
	name = event.data.param1;
	
	console.log("Making the following element visible: " + name);
	
    document.getElementById("options_list").style.display = "none";
    document.getElementById("search_list").style.display = "none";
    document.getElementById("search_text_all_fields").style.display = "none";
    document.getElementById("list_by_group").style.display = "none";
    document.getElementById("list_all").style.display = "none";
    document.getElementById("multi_search").style.display = "none";
    document.getElementById("search_replace_all_fields").style.display = "none";
    document.getElementById("emtpy_field_search").style.display = "none";
    
    document.getElementById(name).style.display = "block";
}


/**
 * 
 * Once the document has fully loaded register handler functions
 *
 * @param NA
 *
 * @return NA
 * 
 */

$(document).ready(function(){
	
	// Register the click handler (#) on id, (.) on class.
	// Note: It is not possible to include parameters for the click
	// handler as this is not a call but merely a pointer to the function
	// itsef. To give parameters to the function, JQuery {} are used
	// that precede the function name. In the braces a list of parameters
	// can be specified that the function can then read via its first
	// parameter.
	$('#show-options-list').click({param1: 'options_list'}, docShowOption);
	$('#show-search-list').click({param1: 'search_list'}, docShowOption);
	$('#show-list-by-group').click({param1: 'list_by_group'}, docShowOption);
	$('#show-list-all-options').click({param1: 'list_all'}, docShowOption);
	$('#show-multi-search').click({param1: 'multi_search'}, docShowOption);
	$('#show-search-text-all-fields').click({param1: 'search_text_all_fields'}, docShowOption);
	$('#show-search-replace-all-fields').click({param1: 'search_replace_all_fields'}, docShowOption);
	$('#show-empty-field-search').click({param1: 'emtpy_field_search'}, docShowOption);

});


