/**
 * home_multi_search.js
 *
 * Javascript functionality to handle the multi-search part of the home
 * page.
 *
 * @version    2.0 2018 10 23
 * @package    DRDB
 * @copyright  Copyright (c) 2018 Martin Sauter
 * @license    GNU General Public License
 * @since      Since Release 1.0
 */

// This variable keeps track of how many search terms are currently
// shown in the multi-search part of the page
var numOfVisibleSearchTerms = 1;

/**
 * 
 * makeAdditionalSearchTermVisible()
 * 
 * This JS function is called when the user presses the '+' button on the
 * right side of a search term in multi-search to get another search term
 * text box.
 *
 * @param NA
 *
 * @return NA
 * 
 */

function makeAdditionalSearchTermVisible () {
	  
	  // Make the next search term visible
	  $('#id_search_term_' + numOfVisibleSearchTerms).show();

	  // Hide the add button in the previous search term well
	  $('#id_add_search_term_button_' + (numOfVisibleSearchTerms-1)).hide();
	  	  
	  numOfVisibleSearchTerms++;

	  // When an additonal search term well is made visible also make
	  // the 'bollean combination' radio buttons visible so the user
	  // can select whether the search terms are combined with "and" or "or"
	  $('#id_bool_combine').show();
	  
}



/**
 * 
 * validateMultiSearchForm()
 * 
 * This JS function is called when the user wants to submit the multi-field
 * search request and validates that the input the user has made leads to a
 * valid database search request.
 *
 * @param NA
 *
 * @return boolean, TRUE is returned if the input was successfully validated 
 *         and the HTTP post triggered by the HTML form is allowed to proceed
 * 
 *         FALSE is returned in case the user input could not be validated. 
 *         Before returning, a dialog box is shown to the user with an 
 *         explanation of the detected error in the request.
 * 
 */

function validateMultiSearchForm() {

	var valResult = false;
	var oneFieldPresent = false;
	
	var formID = $(this).attr('id');
	
	// Only validate the 'multi-select-form
	console.log("Form ID is: " + formID);
	if (formID != 'multi-select-form') {
		return true;
	}

	// loop over each combination of db field selector, text input and 
	// radio button combination
	valRestult = $(".class_db_multi_field" ).each(function(index) {

		// get user input for the current multi-search database field 
		// HTML selector drop-down list
		var selectedField = $(this).val();

		// In addition to the field selector, input field and radio buttons
		// need to be checked. They are addressed with HTML class="" 
		// parameters. Assemble assemble the class names and use them
		// below in jQuery statements.
		var fieldStr = ".class_db_field_string_" + index;
		var notLike = ".class_not_like_" + index;
		var empty = ".class_empty_" + index;

		// Debug output
		/* 
		console.log(index + ": " + $( this ).val());		  
		console.log($(fieldStr).val());

		if ($(notLike).is(':checked')) {
			console.log("Not-Like is CHECKED");
		} else {
			console.log ("Not-Like is NOT CHECKED");
		}

		if ($(empty).is(':checked')) {
			console.log("empty is CHECKED");
		} else {
			console.log ("Empty is NOT CHECKED");
		}
		*/		

		// If the current text field only contains a space or two, consider
		// it as empty and delete the spaces.		
		if ($(fieldStr).val() == " " || $(fieldStr).val() == "  ") {
			$(fieldStr).val("");
		}

		// Check if the user didn't select a database field but has put in 
		// a search string or clicked on one of the two radio buttons
		if (selectedField == "-") {
			if ($(fieldStr).val() != "" ||
				$(notLike).is(':checked') ||
				$(empty).is(':checked')) {

				errorText = "<p>Error in search term " + (index + 1) + ":</p>" + 
				"<p>This can be fixed by one of the following actions:</p>" +
				"<ul><li>Selecting a database field (e.g. 'Numérotation' " +
				"or 'Titre')</li>" + 
				"<li>Deleting the search text</li>" + 
				"<li>By unchecking 'Not Like' and/or 'EMPTY'</li></ul></p>";  

				$("#multi_search_error_text").html(errorText);
				$("#multi_search_validation_error_box").modal('show');				   

				valResult = false;
				return false; // abort this and all other .each() iterations
			} 

		}

		// Check if the user has entered a text and checked the EMPTY radio
		// button
		if (selectedField != "-") {
			if ($(fieldStr).val() != "" &&
				$(empty).is(':checked')) {
				errorText = "<p>Error in search term " + (index + 1) + ":</p>" +
				"<p>Search for EMPTY can't be combined with a search text. " + 
				"Please uncheck 'EMPTY' or delete the search text.</p>";

				$("#multi_search_error_text").html(errorText);
				$("#multi_search_validation_error_box").modal('show');
				
				valResult = false;
				return false; // abort this and all other .each() iteration
			}

		}

		// Check if a field has been selected but there is no text and EMPTY
		// has not been not bene selected
		if (selectedField != "-") {
			if ($(fieldStr).val() == "" &&
				$(empty).is(':checked') == false) {

				errorText = "<p>Error in search term " + (index + 1) + ":</p>" + 
				"<p>A database field has been selected but no search text has " +
				"been entered. This can be fixed by on of the following actions: </p>" + 
				"<ul><li>Entering a search term.</li>" +
				"<li>Setting the field to '-' to disable this search term.</li>" + 
				"<li>Clicking on EMPTY to search for " +
				"<?php echo (DatabaseConfigStorage::getCfgParam('doc-name-ui-plural'));?> in which this " +
				"field is empty.</li></ul></p>";

				$("#multi_search_error_text").html(errorText);
				$("#multi_search_validation_error_box").modal('show');

				valResult = false;
				return false; // abort this and all other .each() iteration
			}	
		}
		
		// If the selector does not contain "-" at least one field
		// has been selected by the user. Therefore set oneFieldPresent variable
		// to true. This variable is checked at the very end to decide
		// if the user has at least slected one database field to search in
		if (selectedField != "-") oneFieldPresent = true; 

		// the search term in this .each() interaction is good or empty, 
		// continue with the next iteration. 
		valResult = true;
		return true;
		
	}); // end of the .each() function iteration loop

	// If an error was found in the .each() loop return false so the user
	// has to correct the input
	if (valResult == false) {
		//console.log ("Validation result is FALSE, aborting validation");
		return false;
	}

	// Final check: Has at least one field been used?
	if (oneFieldPresent == false) {

		errorText = "At least one field (e.g. 'Numérotation' or 'Titre') " + 
		            "must be selected!";
		$("#multi_search_error_text").html("<p>" + errorText + "</p>");
		$("#multi_search_validation_error_box").modal('show');
		
		return false;
	}

	// All checks out
	return true;
}       


/**
 * 
 * Once the document has fully loaded register handler functions
 *
 * @param NA
 *
 * @return NA
 * 
 */

$(document).ready(function(){
	
	// Register the click handler (#) on id, (.) on class.
	$('.add-term').click(makeAdditionalSearchTermVisible);
	
	$("form").submit(validateMultiSearchForm);

});