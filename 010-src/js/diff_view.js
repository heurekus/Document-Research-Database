/**
 * diff_view.js
 *
 * Javascript code for keys and swipe action to go to the previous/next
 * change. 
 *
 * @version    2.0 2018 10 24
 * @package    DRDB
 * @copyright  Copyright (c) 2018 Martin Sauter
 * @license    GNU General Public License
 * @since      Since Release 1.0
 */


//Catch keyboard events to go to the next or the previous change when
//the arrow right / arrow left key is pressed
document.onkeydown = checkKey;

function checkKey(e) {

	e = e || window.event;
	
	if (e.keyCode == '37') {
	  // left arrow (back)
	  prevDiffUrl = $("#prev_diff_url").text();  
	  window.location.href = prevDiffUrl;    
	}
	else if (e.keyCode == '39') {
	  // right arrow (forward)
	  nextDiffUrl = $("#next_diff_url").text();
	  window.location.href = nextDiffUrl;
	}

}

//==========================================================================
//The following code deals with mobile and touch screens. Some functionality
//is only activated on touch devices
//==========================================================================

isMobile = false;
//Note: /xxx/i is a regular expression, '.test' tests for true or
//false against the variable in the round brakets (userAgent...)
if (/Android|iPhone|iPad|iPod/i.test(navigator.userAgent)) isMobile = true;

//
//Activate swipe left / right functionality if we are on a touch screen device
//Part of jquery.touchSwipe.min.js
//
if (isMobile == true) {
	$(function() {      
	    //Keep track of how many swipes
	    var count=0;
	    //Enable swiping...
	    $("#swipe").swipe( {

	      swipeRight:function(event, direction, distance, duration, fingerCount) {
	    	  prevDiffUrl = $("#prev_diff_url").text();  
	    	  window.location.href = prevDiffUrl;
	      },
	      
	      swipeLeft:function(event, direction, distance, duration, fingerCount) {
	    	  nextDiffUrl = $("#next_diff_url").text();
	    	  window.location.href = nextDiffUrl;
	      }
	    });
	  });
} // end of swipe activation

/**
 * windowClose()
 *
 *
 * @param none
 *
 * @return none
 */

function windowClose() {
	window.close();
}


/**
 * diffHelpButtonHandler()
 *
 * Opens a new window with the help page for the diff view page
 *
 * @param none
 *
 * @return none
 */
function diffHelpButtonHandler() {
	
	// Open help in a new window instead of in a tab so the modal
	// print dialog box remains open.
	window.open('/drdb-manual/ChangeHistgory/', 
                'newwindow', 
                'left=100,top=100');
	
}

/**
*
* Misc stuff that needs to be done once the page is fully loaded
*
* @param none
*
* @return none
*/
	
$(document).ready(function(){
	
    // Add event listeners for left column menu buttons
	$('#closetab').click(windowClose);
	$('#closetab_menu').click(windowClose);

	$('#diff-help').click(diffHelpButtonHandler);
});




