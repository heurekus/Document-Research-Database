<?php
/**
 * index-print-raw.php
 *
 * No header and footer info, just the raw output from the page handler
 * PHP file. This is required for file downloads.
 *
 * @version    1.0 2019-02-12
 * @package    DRDB
 * @copyright  Copyright (c) 2014 Martin Sauter
 * @license    GNU General Public License
 * @since      Since Release 1.0
 */

ob_start();

// Before going any further check if the web server has authenticated a user.
if (!isset($_SERVER['PHP_AUTH_USER'])) {
	echo '<p>ERROR: No user authentication configured e.g. in apache2.conf ' .
			'or .htaccess. Aborting...</p>';
	exit;
}

require_once 'includes/init.php';

$executionStartTime = microtime(true);
loadContent('content', 'home');
$executionEndTime = microtime(true);

$log = new Logging();
$executionTime = round($executionEndTime - $executionStartTime, 4);
$log->lwrite("Execution Time: " . $executionTime . ' seconds');
    
