## A Helm Chart for DRDB to deploy to Kubernetes

**Important: This is experimental only! Do not use in production**

## How to use

 * The helm-drdb directory contains all files of the Helm chart. 

 * To deploy DRDB into a Kubernetes cluster, an Ingress component needs to be present in the cluster already. This Helm chart adds an ingress rule for a given domain name. 

 * The Helm chart is installed as follows:

```
helm install drdb ./helm-drdb --set Domain.name=k8s.doc-rdb.de
```

 * Note: Use your own domain name and make sure it points to the ingress of the Kubernetes cluster you use!

 * After the pods are up and running, initialize the DRDB database as follows. In this example, the domain name is used that was given to the helm install command above:

```
http://k8s.doc-rdb.de/drdb/index.php?content=bs_first_use_init
```

 * For initial username and password, see the general installation instructions: https://demo.doc-rdb.de/drdb-manual/installation-docker/

 

 ## Background Info

  * This helm chart was created by converting the special docker-compose.yml file that uses the Docker Hub DRDB image with 'kompose'. 
  
  * After conversion, helm specific variables were added to the yaml files so several instances of DRDB can be deployed (with different domain names) into a single Kubernetes cluster.


## Permanent Virtual Storage (PV) Requirements

 * Each instance of DRDB in a cluster requires 5 PV claims! 
 
 * Each is set to a size of 1 GB in Helm's values.yaml. Smaller values are not accepted by some managed k8s instances. 
 
 * Some instances will automatically allocate much larger sizes, since its the minimum on those clusters. DigitalOcean, for example, allocates 10 GB as minimum PV volume size.

## Update the Deployments when a new 'latest' Image is Availabe

 * To be done once a new DRDB image with tag 'latest' has been pushed to Docker Hub.

 * Helm 'release name' in the example below is 'drdb' (as in 'helm install' above):

```
k rollout restart deployment.apps/drdb-doc-rdb
k rollout restart deployment.apps/drdb-db
```

## Beware: No Https Encryption

 * Currently, the helm chart only install an ingress rule for non-encrypted port 80 http requests! Https support of the ingress rule is not yet implemented.
 