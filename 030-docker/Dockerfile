FROM ubuntu:22.04

MAINTAINER Martin <martin.sauter@wirelessmoves.com>

ENV TZ=Europe/Berlin
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

# RUN apt-get update 
# Note, not installing SSL stuff. Use a reverse proxy for TLS/SSL access
RUN apt update && apt install -y unzip apache2 php libapache2-mod-php php-gd php-zip \
    php-mysql php-mysqlnd php-dom htop nano iputils-ping net-tools


RUN a2enmod rewrite headers
RUN a2enmod php8.1

ENV APACHE_RUN_USER  www-data
ENV APACHE_RUN_GROUP www-data
ENV APACHE_LOG_DIR   /var/log/apache2
ENV APACHE_PID_FILE  /var/run/apache2/apache2.pid
ENV APACHE_RUN_DIR   /var/run/apache2
ENV APACHE_LOCK_DIR  /var/lock/apache2
ENV APACHE_LOG_DIR   /var/log/apache2

RUN mkdir -p $APACHE_RUN_DIR
RUN mkdir -p $APACHE_LOCK_DIR
RUN mkdir -p $APACHE_LOG_DIR

# Extend the PHP session lifetime as some users might lock a db entry for
# editing for a long time
RUN sed -i 's/session.gc_maxlifetime = 1440/session.gc_maxlifetime = 172800/g' /etc/php/8.1/apache2/php.ini

# Enable PHP error logging into /var/log/apache...
RUN echo "" >> /etc/php/8.1/apache2/php.ini
RUN echo "log_errors = On" >> /etc/php/8.1/apache2/php.ini

# Set max upload file size. Instead of overwriting, put an exta config line at
# the end of the file.
RUN echo "upload_max_filesize = 500M" >> /etc/php/8.1/apache2/php.ini
RUN echo "post_max_size = 500M" >> /etc/php/8.1/apache2/php.ini

# Delete the default index.html file
RUN rm /var/www/html/index.html

# Copy the DRDB files
RUN mkdir -p /var/www/html/drdb
COPY ./install-files/00-drdb-installation.tar.gz .
RUN tar xvzf ./00-drdb-installation.tar.gz --directory /var/www/html/drdb/
RUN rm ./00-drdb-installation.tar.gz

# Copy the web browser javascript libraries required
COPY ./install-files/01-drdb-js-libraries.tar.gz .
RUN tar xvzf ./01-drdb-js-libraries.tar.gz --directory /var/www/html/

# Copy documentation html files
COPY ./install-files/02-docdb-manual.tar.gz .
RUN mkdir -p /var/www/html/drdb-manual
RUN tar xvzf ./02-docdb-manual.tar.gz --directory /var/www/html/drdb-manual

# Copy index.html that forwards access to the root directory to /drdb
COPY ./install-files/index-page-for-redirect.html /var/www/html/index.html

RUN chown -R www-data:www-data /var/www

# Add basic authentication (HTTP digest) to the apache config file
COPY ./install-files/apache-cfg-additions.txt .
RUN cat ./apache-cfg-additions.txt >> /etc/apache2/apache2.conf

# Copy default password file into the image in a place where it can
# be mapped to the outside
RUN mkdir /drdb-pwd-dir
COPY ./install-files/pwd.txt /drdb-pwd-dir
RUN chown -R www-data:www-data /drdb-pwd-dir

# Now copy all files that should be exposed to an external volume
# to a temporary directory. run.sh below will check if these
# files exist when the container is started. If not the files
# will be copied back in this shell file!
RUN mkdir /x-temp
RUN mkdir /x-temp/drdb-pwd-dir
RUN mkdir /x-temp/config
RUN mkdir /x-temp/images
RUN mkdir /x-temp/templates
RUN cp /drdb-pwd-dir/* /x-temp/drdb-pwd-dir
RUN cp /var/www/html/drdb/config/* /x-temp/config
RUN cp -r /var/www/html/drdb/images/* /x-temp/images
RUN cp -r /var/www/html/drdb/templates/* /x-temp/templates

EXPOSE 80

# copy run.sh which will copy config files into volume directories
# if these files do not exist there and then starts Apache
COPY ./install-files/run.sh .
RUN chmod +x ./run.sh

CMD ["/bin/bash", "run.sh"]
