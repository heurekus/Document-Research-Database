#!/bin/bash

# Copy files to paths that should be used as a volume outside
# Option -n is used to NOT overwrite config files that already exist in the
# volume paths (i.e. init only!)

cp -n /x-temp/drdb-pwd-dir/* /drdb-pwd-dir
chown -R www-data:www-data /drdb-pwd-dir

cp -n /x-temp/config/* /var/www/html/drdb/config
chown -R www-data:www-data /var/www/html/drdb/config

cp -n /x-temp/images/* /var/www/html/drdb/images
chown -R www-data:www-data /var/www/html/drdb/images

cp -n /x-temp/templates/* /var/www/html/drdb/templates
chown -R www-data:www-data /var/www/html/drdb/templates

# Uploads directory doesn't have templates but should be set to 
# www-data as the owner anyway
chown -R www-data:www-data /var/www/html/drdb/uploads

# Forward DRDB logging output to Docker's logging system (stdout of PID 1)
tail -F /var/www/html/drdb/log/drdb-logfile.txt >> /proc/1/fd/1 &

# Forward Apache and PHP errors to Docker's logging sytstem
tail -F /var/log/apache2/error.log >> /proc/1/fd/1 &

# Now start the web server
/usr/sbin/apache2 -D FOREGROUND

