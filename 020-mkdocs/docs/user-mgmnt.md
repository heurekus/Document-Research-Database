#User Management

Creating, modifying permissions and passwords, and deleting a user can be done by any user with administrative priviledges on the user management page. It can be reached from the Main page via the 'User Management' button

![Screenshot](img/user-management-1.png)

![Screenshot](img/user-management-2.png)

Note: For legacy reasons ```config/user-permissions-config.php``` contains an array with permissions and an array with users. Do NOT modify the permissions array (despite being located in the config folder. The users array is only used during the first initialization of the startup and configures the first admin user. Once configured, the name and permissions of the admin user specified in this array are no longer used!
