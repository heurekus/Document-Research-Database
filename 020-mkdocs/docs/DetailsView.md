#Details View

When clicking on a database record in a list or search result, a new tab is opened that shows the content of the database record. The record can then be viewed or modified with the following options available:

![Screenshot](img/details-view.png)

### Editing a field

 * On desktop devices, a single field can be edited by double clicking on the text. 

 * On touch devices a single tap on the text has the same effect. 

 * While the editor is open for a field, the record is locked. Others are still able to view the record but are prevented from editing the record. When the save button is pressed, the updated text is written to the database. 

 * In addition the new text is also saved in the 'change history'. Previous versions of a field are shown when pressing the little 'hour' icon at the bottom right of a field.

![Screenshot](img/edit-single.png)

### Editing the whole record

 * In some cases several fields might have to be edited in one editing session. The 'Edit' button on the opens editors for all fields. 

 * Note: If the single record view has been configured to only show a selection of fields instead of all fields (see 'Modify the View' below), the editor will also only show these fields. 

### Goto Next/Previous Record

Going to the next or previous record can be done by:

 * Clicking on the icons on the left

 * By using the left/right arrow buttons on the keyboard

 * With right/left swipe gestures on touch devices.

### Modify the View

 * To limit the amount of data shown in detail view it is possible to select the fields and the order in which they are shown. Clock on 'Configure View' to change the settings. Once done the settings are used all detail views that are opened until changed again. 

 * To temporarily show all fields, click on the 'Toggle View All' button. This setting only applies to this detail view.

 * To show or hide empty fields clock on the 'hide/show empty' button. The setting is applied to this and all future detail view.

### Export The Current Record

There are two options to export the current record:

 * Quick Export: Export the record into a Libreoffice Writer document with simple formatting.

 * Formatted Export: Export the record into a Libreoffice Writer document with individual formatting per field. (see [Export to Calc and Writer](export.md))

### Delete the Record

The record can be deleted by clicking on the 'Delete' button. A configuration dialog box will then be shown. If confirmed, the record and its version history will be deleted. WARNING: The record is fully deleted and can not be restored (unless the system administrator has previously made a backup).
