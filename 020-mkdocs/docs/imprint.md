#Imprint

##Responsible for this Website

Martin Sauter

Erftstr. 1

50672 Colgone

Germany

email: gsmumts at gmx.de

##Privacy

The web server stores information about page requests and IP addresses of the requesters in a log file. This information is required for debugging and for countering attacks on the website but are otherwise not analyzed. Logging information is deleted after 30 days. This documentation website has no user accounts and no tracking cookies. Therefore, I do not consider the log files to contain personal data. Apart from the log file, no data is saved or analyzed on this documentation website for the DRDB project.

