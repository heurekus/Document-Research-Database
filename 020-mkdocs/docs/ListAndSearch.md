#Finding Records In The Database

There are two ways to find document records in the database, List and Search. Both options are available by clicking on the corresponding button in the main menu. The main menu can be reached from any page by clicking on the middle header image.

##List

Currently, the following document record lists can be shown:

 * **All**: List all document records in the database. 

 * **One or More Categories**: Show a partial list of all records based on the grouping of records that has been defined in *id-structure.php*. **Optionally**, it is possible to further limit the list to a certain year or phrase in the first field of the records in case the first field contains a year or phrase. If this optional field is used, then only records of the category or categories selected in which the specific search year/phrase is found are part of the result. In addition, records that immediately follow these records that have *an empty* first field are also part of the search result! This is helpful, for example, when a document has several additional records that describe something inside the document and should thus be related with the overall document.

 * **With Colored Text**: List of all documents that contains colored text. Often, text is colored to indicate that there is still work to be done at this location. In this case this option is useful to find all database records which still require some work.

##Search

Currently, the following types of database searches are supported:

 * **Search in Individual Fields**: The first and most versatile kind of search is to search for strings in different record fields. When this search dialog is first opened, a single input line is shown and one field name can be selected that will be searched. To search in more than one field, click on the '+' button on the left as shown in the screenshot below to add another input line for the same or a different field. It's also possible to search for fields that must be empty or not empty and search terms and can be 'and/or' combined. Case sensitive search is also possible.

![Screenshot](img/multi-search.png)

 * **Search Term in All Fields**: The simplest search method is to search for a string in **all** record fields.

 * **Records With Given Fields Empty**: I many cases it can be helpful to get a list of document records where a certain field is empty. 

 * **Search and Replace**: And finally, it is possible to search for text in all fields and replace the text with other text specified in the search. In addition, a text style such as bold and italics can be added or removed in the new text. When using the search/replace functionality, the result list will show the records and fields in which the text that was searched for was found and how the field would look like if the text was replaced. To proceed, click on the 'Replace' button at the end of the result list.


##Configure Which Fields Will Be Shown

A configuration option that is present in many list and search options is to not only show the fields in which a result was found but also other fields which are present in the record. This is useful, for example, to better judge the context of the result list. Once additional fields are selected for display in result lists, they will be shown until unselected before making another search.

![Screenshot](img/show-fields-selection.png)
