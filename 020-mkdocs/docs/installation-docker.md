# **Document Research Database** Installation Instructions (Docker)

There are two ways of installing the Document Research Database (DRDB):

 * **Option 1:** Installation in a Docker container (very simple, recommended)

 * **Option 2:** Installation on a server with manual configuration of Apache, Mysql, certificates, database creation, etc. etc.

This page describes the Docker approach.

# Prerequisites

This project uses ***docker*** and ***docker-compose***. Installation instructions can be found [here](https://blog.wirelessmoves.com/2021/01/dockerize-me-my-personal-docker-bootstrap-voyage-part-1.html).

# Installation

For a test installation without SSL encryption, get the latest docker compose file, rename it and adapt the domain name used:

```text

cd ~
mkdir doc-db-docker
cd doc-db-docker
wget https://codeberg.org/heurekus/Document-Research-Database/raw/branch/master/030-docker/docker-compose.yml.sample
mv docker-compose.yml.sample docker-compose.yml

# Edit docker-compose.yml:
#
# If the Docker container runs locally, leave DRDB_VIRTUAL_HOST set to 'localhost'
# Otherwise, set DRDB_VIRTUAL_HOST to either the IP address of the server or
# the domain name assigned to it.
#

# Start the instance (container images will automatically be pulled from Docker Hub):
docker-compose up -d

```

 * When run for the first time, it will take a few seconds for a demo database to be created. 

 * Once startup is complete, the installation can be reached on TCP port 8500.

 * Finally, the database needs to be initialized by calling the following URL with ```localhost``` or the ```IP address``` of the server that runs the Docker container:

```text
http://localhost:8500/drdb/index.php?content=bs_first_use_init
```
 
 * At this point the installation is fully usable via the network and localy via:

```text
 http://localhost:8500/
```

 * Preconfigured **username/password**: admin/helloworld

 * For details how to create a database with your own fields, etc. etc., have a look at the other entries in the 'installation section'.

## Persistent data storage, backup and restore

 * When an instance is started for the first time, a ```volume``` directory is created in the doc-db-docker directory. Here, all configuration data, the database, templates, uploaded files, etc., are stored.

 * A backup can be made of the complete installation with all data by shutting down the container (docker-compose down) and then backing up the complete doc-db-docker directory. Use tar to preserve access permissions of the files.

 * The database can be restored on the same or on another server by extracting the backup archive. If a 'volume' directory is present (as in this backup/restore scenario, the existing database and configuration will be used!

 * **Important**: File attachements are stored in ```volume/drdb-uploads```. If a DRDB instance was installed before the functionality was added (v24.0.0, 14. July 2022), add this volume manually to the docker-compose.yml file before any file uploads! If the mapping is missing, all uploaded files will be lost when the container is restarted. For new installations, the ```docker-compose.yml.sample``` file already contains the mapping. If many and/or large files are uploaded, this folder can become rather large. Therefore, ensure that enough storage is available on the host.

## TLS/SSL encryption
 
 * For a production deployment, it is strongly recommended to use TLS/SSL encryption between the browser and the web server (https).

 * The web server in the container does NOT provide any SSL encryption for web requests.

 * Adding TLS/SSL encryption is done by using a reverse-proxy in front of the container. A project that has been tested [can be found here](https://github.com/evertramos/docker-compose-letsencrypt-nginx-proxy-companion). Another option (but untested) might be the [Traefik project](https://doc.traefik.io/traefik/getting-started/quick-start/).

