#Delete and Recreate the Database

At the beginning of the project it might be necessary to iteratively create the database and to delete, add and change fields, field names, etc., or to first use the demo configuration and then slowly adapt it to one's specific needs. Whenever database fields are added or deleted in '*database-structure.php*', the SQL database and all existing records used by the DRDB project have to be deleted and recreated. This can be done as follows:

## Option 1: Docker

 * Delte the database volume

```text
docker-compose down
rm -rf volumes/db_data
docker-compose up
```

 * Initialize the database again

```text
http://localhost:8500/drdb/index.php?content=bs_first_use_init
```

 * Done!


## Option 2: Traditional Installation


### Delete the old SQL database

To delete the DRDB database and all its tables, open a shell window and do the following:

```text
sudo -s
mysql -u root 

# In the mysql console:

drop database drdb_example;

```

### Create a new SQL database

```text
# While still in the mysql console:

create database drdb_example;
```

### Initialize the new SQL database

To create the project tables in the new database again, use the following URL in the web browser:

```text
https://192.168.1.42/drdb/index.php?content=bs_first_use_init
```

