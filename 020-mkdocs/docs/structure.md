# Database Structure

As every document research project is different, the database structure of DRDB is flexible and most aspects of the database structure can be changed after installation in the web frontend.

Parameters required for the initial installation and parameters that can not yet be changed in the web frontend can be found in *config/database-structure.php* 

Database fields, their names, their configuration, etc. are contained in in the *$doc_db_description* array. In the past, it was necessary to modify this arry in the configuration file. In the meantime, however, it is strongly advised NOT TO MODIFY THIS array in the configuration file but to use the web frontend instead after initial installation. For details see [here](ManageDatabaseFields.md).



## The Structure of a Database Record

**The following background information is only required for a deeper understanding of the internal data structure (not required for initial installation).**

The database structure that is used to INITIALIZE a DRDB project can be found in *config/database-structure.php*. Once the project is initialized, database structure changes are stored in the database config table itself, and this array is no longer used. For initialization purposes, the array is structured as follows:

 * Each line contains a field that is to be displayed. 

 * The order of this array determines the default order of fields shown on in the GUI.

 * The first parameter (the named index) represents the name of the database field as shown in the GUI It can contain any character, including space and special characters.

 * The GUI name parameter then references an array with three parameters in each line:

 * 1st reference parameter: Name of the field in the database. It **must not** contain spaces, special characters, etc.)
  
 * 2nd reference parameter: SHOW_FIELD: By default all fields are visible (=1) but the configuration can be changed to hide the field by setting this field to 0.
  
 * 3rd reference parameter: PRINT_INLINE: The content of a field can be printed in a separate row (default) or, if it is short, it can also be printed in-line (together with the fieldname) when exporting to Libreoffice. Can be 'true = 1' for inline printing or 'false = 0' for a separate line.


## Example Configuration

The following array is taken from the example installation. The names are not really very useful for a practical applications, as all GUI field names start with the word 'DEMO' and all database field names are numbered consecutively for simplicity's sake. Note, for example, that GUI field names can contain spaces while database field name MUST NOT contain spaces and any kind of special characters!

```
// field constants for the sub-array contents
define ('DB_FIELD_NAME', 0);
define ('SHOW_FIELD', 1);
define ('PRINT_INLINE', 2);

// Array that describes which fields to display. Have a look at the constants 
// directly above for information on what the numbers in the sub-array 
// represent.
$doc_db_description = array(
		'Demo Field 01'  => array('F0K', 1, 0),
		'Demo Field 02'  => array('F0A', 1, 0),
		'Demo Field 03'  => array('F0B', 1, 0),
		'Demo Field 04'  => array('F0C', 1, 0),
		'Demo Field 05'  => array('F0D', 1, 0),
		'Demo Field 06'  => array('F0E', 1, 0),
		'Demo Field 07'  => array('F0F', 1, 0),
		'Demo Field 08'  => array('F0G', 1, 0),
		'Demo Field 09'  => array('F0H', 1, 0),
		'Demo Field 10'  => array('F0I', 1, 0),
		'Demo Field 11'  => array('F0J', 1, 0),
		'Demo Field 12'  => array('F0L', 1, 0),
		'Demo Field 13'  => array('F0M', 1, 0),
		'Demo Field 14'  => array('F0N', 1, 0),
		'Demo Field 15'  => array('F0O', 1, 0),
		'Demo Field 16'  => array('F0P', 1, 0),
		'Demo Field 17'  => array('F0Q', 1, 0),
		'Demo Field 18'  => array('F0R', 1, 0),
		'Demo Field 19)' => array('F0S', 1, 0),
		'Demo Field 20'  => array('F0T', 1, 0),
		'Demo Field 21'  => array('F0U', 1, 0),
		'Demo Field 22'  => array('F0V', 1, 0),
		'Demo Field 23'  => array('F0W', 1, 0),
		'Demo Field 24'  => array('F0X', 1, 0),
		'Demo Field 25'  => array('F0Y', 1, 0),
		'Demo Field 26'  => array('F0Z', 1, 0),
		'Demo Field 27'  => array('FAA', 1, 0),
		'Demo Field 28'  => array('FAB', 1, 0),
		'Demo Field 29'  => array('FAC', 1, 0),
		'Demo Field 30'  => array('FAD', 1, 0),
		'Demo Field 31'  => array('FAE', 1, 0),
		'Demo Field 32'  => array('FAF', 1, 0),
		'Demo Field 33'  => array('FAG', 1, 0),
		'Demo Field 34'  => array('FAH', 1, 0),
		'Demo Field 35'  => array('FAI', 1, 0),
);

```
