# Change Configuration Parameters

There are a number of configuration parameters such as the site name, footer text, etc. that are initially configured via config.php. They can be changed via an admin web page as well. Once they are changed via the GUI, values in config.php are disregarded.

## Administration Management Page

![Screenshot](img/parameter-change-1.png)


## Parameter Management Page

![Screenshot](img/parameter-change-2.png)


