# Add File Attachments to a Field

Users with 'edit' permissions can add images, PDF documents, audio files, video files, etc. to any field of a database record by clicking on the 'file attach' icon of a field:

![Screenshot](img/file-attachements.png)

Attached files are opened in a new web browser tab or offered for download depending on the file type. The maximum file size that has been tested so far is 100 MB. The exact limit depends on the project setup. For details see [here](FileSizeLimits.md).

