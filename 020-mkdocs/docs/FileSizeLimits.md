# Upload File Engineering Considerations

### Maximum File Size

The maximum file size is currently hard coded to **500 MB** per file in _/includes/classes/fileuploads.php_. However, other limits might prevent uploads of such large files. Examples are file upload limits in a potential reverse proxy setup used in front of this project and general PHP limits of this project itself. The maximum file size tested so far is 100 MB.

### Location of Uploaded Files

All files that are uploaded into a project are stored in _volumes/drdb_uploads_. For each database record, a directory is created in this path. The directory name is the hash ID of the database record (which is also used to create a permalink to a record). In this database record directory, sub-directories are created for each field name. This way, backups of file attachements can be made together with configuration values and the SQL database which are also stored in other sub-directories of _volumes_ .