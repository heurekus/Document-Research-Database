#!/bin/bash

TAR_FILE="02-docdb-manual.tar.gz"

if [[ $EUID -ne 0 ]]; then

   echo
   echo "ERROR: This script must be run as root, please try again!"
   echo
   exit 1
fi

cd ~/Documents/docdb
mkdocs build
cd ~/Documents/docdb/site
tar cvzf ~/development/doc-db-docker/install-files/$TAR_FILE .
